package scf;

import com.google.common.base.Stopwatch;
import com.nx.platform.es.common.utils.PathUtil;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @author zhanghuaren
 * @date 2018/08/16
 */
public class Es6Test {

    public static void main(String[] args) throws Exception {
        String n = "200";
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", n);
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost("10.148.15.235", 5500, "http")));
        highLevelApiMatchAllTest(restHighLevelClient);
        System.out.println("---");
        cost("HighLevelApi MatchAll", 100, () -> highLevelApiMatchAllTest(restHighLevelClient));
        cost("HighLevelApi Complex", 100, () -> highLevelApiComplexTest(restHighLevelClient));
        System.out.println("---");
        cost("HighLevelApi MatchAll", 1000, () -> highLevelApiMatchAllTest(restHighLevelClient));
        cost("HighLevelApi Complex", 1000, () -> highLevelApiComplexTest(restHighLevelClient));
        System.out.println("---");
        parallelCost("parallel-" + n + "-HighLevelApi MatchAll", 1000, () -> highLevelApiMatchAllTest(restHighLevelClient));
        parallelCost("parallel-" + n + "-HighLevelApi Complex", 1000, () -> highLevelApiComplexTest(restHighLevelClient));
        System.out.println("---");
        restHighLevelClient.close();
    }

    public static long highLevelApiMatchAllTest(RestHighLevelClient client) {
        try {
            SearchRequest searchRequest = new SearchRequest("gift_v6");
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.matchAllQuery());
            searchRequest.source(searchSourceBuilder);
            SearchResponse response = client.search(searchRequest);
            return response.getTook().getMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public static long highLevelApiComplexTest(RestHighLevelClient client) {
        try {
            SearchRequest searchRequest = new SearchRequest("gift_v6");
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            QueryBuilder query = QueryBuilders.boolQuery()
                    .filter(QueryBuilders.termsQuery("status", "1", "3"))
                    .filter(QueryBuilders.termQuery("product_type", 1))
                    .filter(QueryBuilders.termQuery("searchable", 1));
            searchSourceBuilder.query(query).sort("refresh_time", SortOrder.DESC);
            searchRequest.source(searchSourceBuilder);
            SearchResponse response = client.search(searchRequest);
            return response.getTook().getMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public static void cost(String desc, int times, Runnable runnable) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        IntStream.range(0, times).forEach(i -> runnable.run());
        System.out.println(desc + "-" + times + " request took avg: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) / (double) times);
    }

    public static void parallelCost(String desc, int times, Runnable runnable) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        IntStream.range(0, times).parallel().forEach(i -> runnable.run());
        System.out.println(desc + "-" + times + " request took avg: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) / (double) times);
    }

    public static void cost(String desc, int times, Callable<Long> runnable) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        System.out.println(desc + "-" + times + " server took avg: " + IntStream.range(0, times).mapToLong(i -> {
            try {
                return runnable.call();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }).average().orElse(0));
        System.out.println(desc + "-" + times + " request took avg: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) / (double) times);
    }

    public static void parallelCost(String desc, int times, Callable<Long> runnable) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        System.out.println(desc + "-" + times + " server took avg: " + IntStream.range(0, times).parallel().mapToLong(i -> {
            try {
                return runnable.call();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }).average().orElse(0));
        System.out.println(desc + "-" + times + " request took avg: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) / (double) times);
    }
}
