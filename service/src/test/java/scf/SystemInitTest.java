//package scf;
//
//import java.io.File;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.nx.arch.common.util.Path;
//import com.nx.arch.nxrpc.client.SCFInit;
//import com.nx.platform.es.system.init.IdcMachineCode;
//
///**
// * 初始化
// */
//public class SystemInitTest {
//
//    private final static Logger logger = LoggerFactory.getLogger(SystemInitTest.class);
//
//    private final static String CONFIG_PATH = Path.getCurrentPath() + "/config";
//
//    private final static String SCF_KEY_PATH = CONFIG_PATH + "/scfkey.key";
//
//    private final static String SCF_CONFIG_PATH = CONFIG_PATH + "/scf.config";
//
//    public void init() {
//        /*
//         * 初始化SCF配置
//         */
//        logger.info("begin scf init");
//        if (logger.isDebugEnabled()) {
//            logger.debug("-------SystemInit config path: " + CONFIG_PATH + "----------");
//        }
//        if (new File(SCF_KEY_PATH).exists()) {
//            SCFInit.initScfKey(SCF_KEY_PATH);
//        }
//        SCFInit.init(SCF_CONFIG_PATH);
//        logger.info("end scf init");
//
//        // TODO 服务内有用到idc生成ID就需要IdcMachineCode，没有就不需要
//        if (!IdcMachineCode.init()) {
//            logger.error("SystemInit desc=loadMachinecode return false, exit system.");
//        }
//    }
//
//}
