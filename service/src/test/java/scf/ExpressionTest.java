package scf;


import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.biz.esspider.expression.Expression;
import com.nx.platform.es.biz.esspider.expression.ValExpression;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author zhanghuaren
 * @date 2018/01/5
 */
public class ExpressionTest {

    @Test
    public void testVal() {
        ValExpression expression = ValExpression.create("flow_control | flow_control_algo");
        System.out.println(expression.eval(ImmutableMap.of("flow_control", 66564, "flow_control_algo", 32)));
    }

    @Test
    public void test() {

        Expression expression;

        expression = Expression.create("searchable = 0 || status != 1");
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 0)));
        Assert.assertTrue(!expression.eval(ImmutableMap.of("searchable", 1)));
        Assert.assertTrue(!expression.eval(ImmutableMap.of("searchable", 1, "status", 1)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 1, "status", 0)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 0, "status", 1)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 0, "status", 0)));

        expression = Expression.create("searchable || status != 1");
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 0, "status", 0)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", 1, "status", 0)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", "AAA", "status", 0)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("searchable", "", "status", 0)));
        Assert.assertTrue(expression.eval(ImmutableMap.of("status", 0)));
        Assert.assertTrue(!expression.eval(ImmutableMap.of("status", 1)));

        expression = Expression.create("timestamp + 3600 * 24 * 60 >= CURRENT_TIME");
        Stopwatch stopwatch = Stopwatch.createStarted();
        for (int i = 0; i < 250000; i++) {
            Assert.assertTrue(!expression.eval(ImmutableMap.of("timestamp", System.currentTimeMillis() - 3600 * 24 * 61)));
            Assert.assertTrue(expression.eval(ImmutableMap.of("timestamp", System.currentTimeMillis() - 3600 * 24 * 59)));
        }
        System.out.println(stopwatch.toString());

        expression = Expression.create("status != 3 || update_timestamp + 15552000000 <= CURRENT_TIME");
        Assert.assertTrue(expression.eval(ImmutableMap.of("status", 3, "update_timestamp", 1499744513963L)));

    }

}
