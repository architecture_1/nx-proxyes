package scf;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.rocketmq.client.consumer.DefaultMQPushConsumer;
import com.alibaba.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import com.alibaba.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.common.consumer.ConsumeFromWhere;
import com.alibaba.rocketmq.common.message.MessageExt;
import com.alibaba.rocketmq.common.protocol.heartbeat.MessageModel;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;

public class RocketMQConsumeTest {

    /**
     * 消费者组名(各个消费者必须不同)
     */
    private static final String CONSUMER_GROUP_NAME = "consumer-group-name";
    /**
     * MQ服务器地址
     */
    private static final String NAME_SRV_ADDR = "name-srv-addr";
    /**
     * 消费者线程数量(最小值)
     */
    private static final String CONSUMER_THREAD_MIN = "consumer-thread-min";
    /**
     * 消费者线程数量(最大值)
     */
    private static final String CONSUMER_THREAD_MAX = "consumer-thread-max";
    /**
     * 消费者消息批量接收最大值
     */
    private static final String CONSUMER_MESSAGE_BATCH_MAX_SIZE = "consumer-message-batch-max-size";
    /**
     * 关注的主题
     */
    private static final String TOPIC = "topic";
    /**
     * 关注的标签
     */
    private static final String TAGS = "tags";

    private static final String DEFAULT_TAGS = "*";
    private static final int DEFAULT_CONSUMER_MESSAGE_BATCH_MAX_SIZE = 1;
    private static final int DEFAULT_CONSUMER_THREAD_MIN = 20;
    private static final int DEFAULT_CONSUMER_THREAD_MAX = 64;
    private static final Pattern SEMICOLON = Pattern.compile(";");

    private static DefaultMQPushConsumer consumer;

    public static void main(String[] args) {
        try {
            Map<String, Object> setting = new HashMap<>();
            setting.put(CONSUMER_GROUP_NAME, "arch-search-es-notifyer-dev");
            setting.put(NAME_SRV_ADDR, "192.168.185.172:9876;192.168.186.131:9876");
            setting.put(TOPIC, "info");
            prepare(setting);
            Thread.sleep(3600000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            shutdown();
        }
    }

    private static void prepare(Map<String, Object> setting) throws MQClientException {

        // base check
        String consumerGroupName = MapUtils.getString(setting, CONSUMER_GROUP_NAME);
        Preconditions.checkArgument(StringUtils.isNotEmpty(consumerGroupName));

        String nameSrvAddr = MapUtils.getString(setting, NAME_SRV_ADDR);
        Preconditions.checkArgument(StringUtils.isNotEmpty(nameSrvAddr));

        String topic = MapUtils.getString(setting, TOPIC);
        Preconditions.checkArgument(StringUtils.isNotEmpty(topic));
        Iterator<String> topicIter = Splitter.on(SEMICOLON).trimResults().omitEmptyStrings().split(topic).iterator();

        String tags = MapUtils.getString(setting, TAGS, "");
        Iterator<String> tagIter = Splitter.on(SEMICOLON).trimResults().omitEmptyStrings().split(tags).iterator();

        int batchMaxSize = MapUtils.getIntValue(setting,
                CONSUMER_MESSAGE_BATCH_MAX_SIZE, DEFAULT_CONSUMER_MESSAGE_BATCH_MAX_SIZE);
        Preconditions.checkArgument(batchMaxSize > 0);

        int consumerThreadMin = MapUtils.getIntValue(setting, CONSUMER_THREAD_MIN, DEFAULT_CONSUMER_THREAD_MIN);
        int consumerThreadMax = MapUtils.getIntValue(setting, CONSUMER_THREAD_MAX, DEFAULT_CONSUMER_THREAD_MAX);
        Preconditions.checkArgument(consumerThreadMin > 0 && consumerThreadMax >= consumerThreadMin);

        // init consumer
        consumer = new DefaultMQPushConsumer(consumerGroupName);
        consumer.setNamesrvAddr(nameSrvAddr);
        while (topicIter.hasNext()) {
            consumer.subscribe(topicIter.next(), Iterators.getNext(tagIter, DEFAULT_TAGS));
        }
        // 新消费者从消息队列头获取数据
        // consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        // 新消费者忽略历史消息，接收接入当时之后的消息
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
        consumer.setMessageModel(MessageModel.CLUSTERING); // 默认集群模式
        consumer.setConsumeMessageBatchMaxSize(batchMaxSize);
        consumer.setConsumeThreadMin(consumerThreadMin);
        consumer.setConsumeThreadMax(consumerThreadMax);

        consumer.registerMessageListener((List<MessageExt> msgs, ConsumeConcurrentlyContext context) -> {
            for (MessageExt msgExt : msgs) {
                String header = msgExt.getUserProperty("MQHeader");
                String body = new String(msgExt.getBody());
                System.out.println("========>\n received new message, header=" + header + ", body=" + body);
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        consumer.start();
    }

    private static void shutdown() {
        if (consumer != null)
            consumer.shutdown();
    }

}
