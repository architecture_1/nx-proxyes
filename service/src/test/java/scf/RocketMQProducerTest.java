package scf;

import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.alibaba.rocketmq.common.message.Message;
import com.alibaba.rocketmq.remoting.exception.RemotingException;
import com.google.common.base.Preconditions;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RocketMQProducerTest {

    /**
     * 组名(各个产者应不同)
     */
    private static final String PRODUCER_GROUP_NAME = "producer-group-name";
    /**
     * MQ服务器地址
     */
    private static final String NAME_SRV_ADDR = "name-srv-addr";
    /**
     * 消息主题
     */
    private static final String TOPIC = "topic";
    /**
     * 消息标签
     */
    private static final String TAGS = "tags";
    /**
     * 发送超时时间
     */
    private static final String SEND_MSG_TIMEOUT = "send-msg-timeout";

    private static final int DEFAULT_SEND_MSG_TIMEOUT = 800;

    private static DefaultMQProducer producer;
    private static String topic;
    private static String tags;

    public static void main(String[] args) {
        try {
            Map<String, Object> setting = new HashMap<>();
//            setting.put(PRODUCER_GROUP_NAME, "esnotifyer-test-1");
//            setting.put(NAME_SRV_ADDR, "192.168.188.63:9876;192.168.188.64:9876");
//            setting.put(TOPIC, "167177youpinYpcstore");
//            setting.put(TAGS, "storeProductChange");

            // pro
//            setting.put(PRODUCER_GROUP_NAME, "esnotifyer-online-1");
//            setting.put(NAME_SRV_ADDR, "10.126.84.166:9876;10.126.84.166:9876");
//            setting.put(TOPIC, "gspu_lowest_price");
//            setting.put(TAGS, "gspu_lowest_price_update_tag");

            // uat
            setting.put(PRODUCER_GROUP_NAME, "esnotifyer-uat-1");
            setting.put(NAME_SRV_ADDR, "10.136.200.166:9876;10.136.200.167:9876");
            setting.put(TOPIC, "193124giftproduct_topic");
            setting.put(TAGS, "storeProductChange");
            prepare(setting);
            send("{\"productId\":\"1126329471177998339\",\"status\":1}");
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            shutdown();
        }
    }

    private static void prepare(Map<String, Object> setting) throws MQClientException {

        String producerGroupName = MapUtils.getString(setting, PRODUCER_GROUP_NAME);
        Preconditions.checkArgument(StringUtils.isNotEmpty(producerGroupName));
        String nameSrvAddr = MapUtils.getString(setting, NAME_SRV_ADDR);
        Preconditions.checkArgument(StringUtils.isNotEmpty(nameSrvAddr));
        topic = MapUtils.getString(setting, TOPIC);
        Preconditions.checkArgument(StringUtils.isNotEmpty(topic));
        tags = MapUtils.getString(setting, TAGS);
        int sendMsgTimeout = MapUtils.getIntValue(setting, SEND_MSG_TIMEOUT, DEFAULT_SEND_MSG_TIMEOUT);
        Preconditions.checkArgument(sendMsgTimeout > 0);

        producer = new DefaultMQProducer(producerGroupName);
        producer.setNamesrvAddr(nameSrvAddr);
        producer.setSendMsgTimeout(sendMsgTimeout);
        producer.start();

    }

    private static void send(String message) throws MQClientException, RemotingException, InterruptedException {
        String header = UUID.randomUUID().toString();
        Message msg = new Message(topic, tags, message.getBytes());
        msg.putUserProperty("MQHeader", header);
        producer.sendOneway(msg);
        System.out.println("Sent message, header=" + header + ", body=" + message);
    }

    private static void shutdown() {
        if (producer != null)
            producer.shutdown();
    }

}
