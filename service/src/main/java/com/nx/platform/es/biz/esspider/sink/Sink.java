package com.nx.platform.es.biz.esspider.sink;

import com.nx.platform.es.biz.esspider.entity.Item;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * @author
 * @date 2018/01/25
 */
public interface Sink {

    /**
     * 初始化Sink
     *
     * @param settings Sink配置
     * @throws Exception
     */
    default void init(Map<?, ?> settings) throws Exception {}

    /**
     * 接收并处理数据，返回处理失败的数据
     *
     * @param items 待处理数据集
     * @return 处理失败数据集
     */
    @NotNull
    Map<Long, Item> accept(Map<Long, Item> items);

}
