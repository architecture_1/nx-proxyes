package com.nx.platform.es.common.utils;

import com.google.common.util.concurrent.Service;
import org.apache.logging.log4j.Logger;

/**
 * @author
 * @date 2018/01/26
 */
public class ServiceListener extends Service.Listener {

    private final Logger logger;
    private final String serviceName;

    public ServiceListener(Logger logger, String serviceName) {
        this.logger = logger;
        this.serviceName = serviceName;
    }

    @Override
    public void starting() {
        logger.info("Starting service({}).", serviceName);
    }

    @Override
    public void running() {
        logger.info("Started service({}).", serviceName);
    }

    @Override
    public void stopping(Service.State from) {
        logger.info("Stopping service({}). Previous state was: {}.", serviceName, from);
    }

    @Override
    public void terminated(Service.State from) {
        logger.info("Service({}) has terminated. Previous state was: {}.", serviceName, from);
    }

    @Override
    public void failed(Service.State from, Throwable failure) {
        logger.info("Service({}) has failed in the {} state.", serviceName, from, failure);
    }

}
