package com.nx.platform.es.biz.query.agg.cache;


import com.nx.platform.es.entity.response.TermsAggResult;

/**
 * @author
 * @date 2018/02/28
 */
public class NullAggCache implements AggCache {

    @Override
    public TermsAggResult get(String logStr, String cacheKey) {
        return null;
    }

    @Override
    public void put(String logStr, String cacheKey, TermsAggResult result, int seconds) {
        // empty ok
    }

}
