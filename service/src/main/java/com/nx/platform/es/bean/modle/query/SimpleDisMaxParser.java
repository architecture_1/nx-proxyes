package com.nx.platform.es.bean.modle.query;


import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;
import java.util.regex.Pattern;

public class SimpleDisMaxParser implements QueryFieldConfigParser {
    private static final Pattern FORMAT = Pattern.compile("[\\w.]+");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        //
        String face = MoreMaps.getString(settings, "face", "");
        String fieldName = MoreMaps.getString(settings, "fieldName", "");
        String clazz = MoreMaps.getString(settings, "class", "");
        String _tie_breaker = MoreMaps.getString(settings, "_tie_breaker", "1");
        if ("".equals(fieldName)) {
            fieldName = face;
        }
        Preconditions.checkState(FORMAT.matcher(face).matches(), "param face empty or incorrect: " + face);
        Preconditions.checkState(FORMAT.matcher(fieldName).matches(), "param fieldName empty or incorrect" + fieldName);
        //
        String defaultValue = MoreMaps.getString(settings, "default", "");
        //
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("face", face);
        builder.put("fieldName", fieldName);
        builder.put("class", clazz);
        builder.put("default", defaultValue);
        builder.put("_tie_breaker", _tie_breaker);
        return builder.build();
    }
}
