package com.nx.platform.es.mq.constants;

/**
 * 各种MQ tag
 */
public interface MqTags {

    // 释放库存
    String TAG_UNLOCK_STOCK = "TAG_UNLOCK_STOCK";

    // 消费库存
    String TAG_CONSUME_STOCK = "TAG_CONSUME_STOCK";

}
