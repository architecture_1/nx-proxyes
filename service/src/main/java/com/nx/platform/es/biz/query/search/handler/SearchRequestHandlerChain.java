package com.nx.platform.es.biz.query.search.handler;

/**
 * 请求处理链
 * <p>
 * Created by  on 2017/4/18.
 */
public interface SearchRequestHandlerChain extends SearchRequestHandler {

    void appendHandler(SearchRequestHandler handler);

}
