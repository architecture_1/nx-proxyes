package com.nx.platform.es.biz.esspider.handler;

import com.nx.platform.es.biz.esspider.resource.DBManager;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author
 * @since 2017年3月22日
 */
public abstract class AbstractDBHandler implements Handler {

    private final String identity;
    private final DBManager dbManager;

    public AbstractDBHandler(String identity, DBManager dbManager) {
        this.identity = identity;
        this.dbManager = dbManager;
    }

    protected NamedParameterJdbcTemplate getJdbcTemplate() {
        return dbManager.acquireJdbcTemplate(identity);
    }

}
