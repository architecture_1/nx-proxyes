package com.nx.platform.es.biz.esspider.source;

import com.google.common.util.concurrent.Service;

/**
 * 数据源服务
 * 
 * @author
 * @since 2017年3月24日
 */
public interface SourceService extends Service {
    // Empty OK
}
