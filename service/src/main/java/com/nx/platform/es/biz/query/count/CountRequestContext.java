package com.nx.platform.es.biz.query.count;


import com.google.common.collect.Table;
import com.nx.platform.es.bean.dto.RequestContext;
import com.nx.platform.es.bean.modle.query.QueryField;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;

import java.util.List;

/**
 * @author
 * @since 2016年10月21日
 */
public class CountRequestContext extends RequestContext {

    private List<QueryField> queryFields;
    private Table<String, StatementOperator, List<String>> queryTable;

    public CountRequestContext(String logPrefix, String entry, String query, String params) {
        super(logPrefix, entry, query, params);
    }

    public List<QueryField> getQueryFields() {
        return queryFields;
    }

    public void setQueryFields(List<QueryField> queryFields) {
        this.queryFields = queryFields;
    }

    public Table<String, StatementOperator, List<String>> getQueryTable() {
        return queryTable;
    }

    public void setQueryTable(Table<String, StatementOperator, List<String>> queryTable) {
        this.queryTable = queryTable;
    }

}
