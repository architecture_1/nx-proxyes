package com.nx.platform.es.biz.query.search.cache;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nx.platform.es.entity.response.SearchResult;

/**
 * 搜索缓存接口
 * <p>
 * Created by  on 2017/4/17.
 */
public interface SearchCache {

    Gson GSON = new GsonBuilder().enableComplexMapKeySerialization()
            .serializeSpecialFloatingPointValues().create();

    String CACHE_KAY_PREFIX = "es:search:";

    default String getIdentity() {
        return this.getClass().getSimpleName();
    }

    /**
     * 获取缓存中的搜索结果
     *
     * @param logStr 日志前缀
     * @param cacheKey 缓存Key
     * @return 缓存结果
     */
    SearchResult get(final String logStr, final String cacheKey);

    /**
     * 查询结果放入缓存
     *
     * @param logStr 日志前缀
     * @param cacheKey 缓存Key
     * @param result 查询结果
     * @param seconds 失效时间
     */
    void put(final String logStr, final String cacheKey, final SearchResult result, final int seconds);

}
