package com.nx.platform.es.biz.esspider.process;

import com.google.common.util.concurrent.Service;

/**
 * @author
 * @since 2017年3月24日
 */
public interface ProcessorService extends Processor, Service {

}
