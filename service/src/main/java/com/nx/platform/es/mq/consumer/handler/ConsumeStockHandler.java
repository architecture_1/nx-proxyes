package com.nx.platform.es.mq.consumer.handler;

import com.nx.platform.es.mq.consumer.ConsumerHandler;
import com.nx.platform.es.mq.constants.MqTags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 消费库存
 */
@Component(MqTags.TAG_CONSUME_STOCK)
public class ConsumeStockHandler implements ConsumerHandler {

    private Logger logger = LoggerFactory.getLogger(ConsumeStockHandler.class);

    /**
     * 执行失败返回false，MQ中间件会重试
     *
     * @param tag
     * @param key
     * @param body
     * @return
     */
    @Override
    public boolean consume(String tag, String key, String body) {

        String logStr = String.format("desc=trademq_%s_%s", this.getClass().getName(), System.currentTimeMillis());
        logger.info("{} trade mq consume start, tag={}, key={}, body={}", logStr, tag, key, body);

//        Map<String, String> paramsMap = JsonUtil.ofMap(body);
//        Long orderId = Long.valueOf(paramsMap.get("orderId"));

        return true;
    }

}
