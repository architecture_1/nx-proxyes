package com.nx.platform.es.biz.esspider.exception;


import com.nx.platform.es.biz.esspider.entity.Message;

/**
 * @author
 * @since 2017年3月25日
 */
public class IllegalMessageException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public IllegalMessageException(Message message) {
        super(String.valueOf(message));
    }
}
