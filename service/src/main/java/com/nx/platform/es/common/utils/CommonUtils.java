package com.nx.platform.es.common.utils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.common.primitives.Ints;
import com.nx.platform.es.system.config.ConfigCenter;
import org.elasticsearch.common.geo.GeoHashUtils;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;

/**
 * @author
 * @since 2016年12月1日
 */
public class CommonUtils {

    public static void main(String[] args) {

        System.out.println(merge(201, 0));

        long now = System.currentTimeMillis();
        System.out.println(now);
        now = (now / 300000) * 300000;
        System.out.println(now);

    }

    public static String toMD5(@NotNull String str) {
        return Hashing.murmur3_128().hashString(str, Charsets.UTF_8).toString();
    }

    public static long merge(int high, int low) {
        return ((long) low & 0xFFFFFFFFL) | (((long) high << 32) & 0xFFFFFFFF00000000L);
    }

    public static String merge(int high, long low) {
        return BigInteger.valueOf((long) high).shiftLeft(64).add(BigInteger.valueOf(low)).toString();
    }

    /**
     * Encode to a level specific geohash string from full resolution longitude,
     * latitude
     */
    public static String geohash(final double lon, final double lat, final int level) {
        return GeoHashUtils.stringEncode(lon, lat, level <= 0 ? 5 : level);
    }

    public static int getMaxAlternatives() {
        Integer maxAlternatives = (Integer) ConfigCenter.getConfig("max_alternatives", Ints::tryParse).orElse(null);
        return (maxAlternatives == null || maxAlternatives <= 0) ? 200 : maxAlternatives;
    }

}
