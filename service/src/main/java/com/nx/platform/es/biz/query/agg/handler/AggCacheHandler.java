package com.nx.platform.es.biz.query.agg.handler;

import com.google.common.collect.Table;
import com.nx.platform.es.biz.query.agg.AggRequestContext;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import static com.nx.platform.es.biz.wrapper.parser.StatementOperator.EQUAL;


/**
 * @author
 * @since 2016年12月1日
 */
public enum AggCacheHandler implements AggRequestHandler {

    INSTANCE;

    @Override
    public void handle(AggRequestContext context) {
        Table<String, StatementOperator, List<String>> queryTable = context.getQueryTable();
        List<String> list = queryTable.get("nocache", EQUAL);
        context.setNocache(CollectionUtils.isNotEmpty(list) && "1".equals(list.get(0)));
    }

}
