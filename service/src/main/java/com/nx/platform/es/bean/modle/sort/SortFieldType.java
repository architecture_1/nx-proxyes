package com.nx.platform.es.bean.modle.sort;

/**
 * @author
 * @since 2016年10月15日
 */
public enum SortFieldType {

    /** 按得分排序 */
    ScoreSort(new SimpleParser(), new ScoreSortHandler()),
    /** 数字字段值排序 */
    NumberFieldSort(new FieldSortParser(), new NumberFieldSortHandler()),
    /** 日期字段值排序 */
    DateFieldSort(new FieldSortParser(), new DateFieldSortHandler()),
    /** 经纬度距离排序 */
    GeoDistanceSort(new FieldSortParser(), new GeoDistanceSortHandler()),
    /** Painless脚本排序 */
    PainlessScriptSort(new PainlessScriptSortParser(), new PainlessScriptSortHandler());

    private final SortFieldConfigParser parser;
    private final SortFieldHandler handler;

    SortFieldType(SortFieldConfigParser parser, SortFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public SortFieldConfigParser getParser() {
        return parser;
    }

    public SortFieldHandler getHandler() {
        return handler;
    }

}
