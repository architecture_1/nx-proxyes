package com.nx.platform.es.bean.modle.agg;

/**
 * @author
 * @since 2016年10月15日
 */
public enum AggFieldType {

    /**  */
    NumberTerms(new SimpleParser(), new NumberTermsHandler());

    private final AggFieldConfigParser parser;
    private final AggFieldHandler handler;

    AggFieldType(AggFieldConfigParser parser, AggFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public AggFieldConfigParser getParser() {
        return parser;
    }

    public AggFieldHandler getHandler() {
        return handler;
    }

}
