package com.nx.platform.es.biz.query.agg.handler;

/**
 * 请求处理链
 * <p>
 * Created by  on 2017/4/18.
 */
public interface AggRequestHandlerChain extends AggRequestHandler {

    void appendHandler(AggRequestHandler handler);

}
