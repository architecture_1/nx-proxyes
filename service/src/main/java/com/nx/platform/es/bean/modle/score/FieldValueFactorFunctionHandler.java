package com.nx.platform.es.bean.modle.score;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.MapUtils;
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class FieldValueFactorFunctionHandler implements ScoreFunctionFieldHandler {

    @Override
    public FilterFunctionBuilder handle(ImmutableMap<String, ?> fieldConfig, Map<String, Object> params) {
        String fieldName = MapUtils.getString(fieldConfig, "fieldName");
        float weight = MapUtils.getFloatValue(fieldConfig, "weight", 0f);
        float missing = MapUtils.getFloatValue(fieldConfig, "missing", 0f);
        FieldValueFactorFunctionBuilder function = ScoreFunctionBuilders
                .fieldValueFactorFunction(fieldName).missing(missing);
        if (weight > 0) {
            function.setWeight(weight);
        }
        return new FilterFunctionBuilder(function);
    }

}
