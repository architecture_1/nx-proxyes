package com.nx.platform.es.biz.query.search.cache;


import com.nx.platform.es.entity.response.SearchResult;

/**
 * @author
 * @date 2018/02/28
 */
public class NullSearchCache implements SearchCache {

    @Override
    public SearchResult get(String logStr, String cacheKey) {
        return null;
    }

    @Override
    public void put(String logStr, String cacheKey, SearchResult result, int seconds) {
        // empty ok
    }

}
