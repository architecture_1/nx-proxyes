package com.nx.platform.es.bean.modle.query;

import com.google.common.collect.ListMultimap;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import com.nx.platform.es.biz.query.search.SearchRequestContext;
import org.elasticsearch.index.query.QueryBuilder;

/**
 * @author
 * @since 2016年10月15日
 */
public enum QueryFieldType {

    /**
     * 字符串(搜索)
     */
    StringMatch(new StringMatchParser(), new StringMatchHandler()),
    /**
     * 字符串(搜索)
     */
    StringMatchShould(new StringMatchParser(), new StringMatchShouldHandler()),
    /**
     * 字符串(匹配)
     */
    StringNotAnalyzed(new SimpleParser(), new StringNotAnalyzedHandler()),
    /**
     * 字符串(多字段搜索)
     */
    StringMultiMatch(new MultiMatchParser(), new StringMultiMatchHandler()),
    /**
     * 字符串(多字段搜索)
     */
    StringMultiMatchShould(new MultiMatchParser(), new StringMultiMatchShouldHandler()),
    /**
     * 布尔
     */
    Boolean(new SimpleParser(), new BooleanHandler()),
    /**
     * 整数
     */
    Number(new SimpleParser(), new NumberHandler()),
    /**
     * 普通数据类型的查询或
     */
    NumberMultiKey(new MultiMatchParser(), new NumberMultiKeyHandler()),
    /**
     * 浮点数
     */
    Float(new SimpleParser(), new FloatHandler()),
    /**
     * 日期
     */
    Date(new SimpleParser(), new DateHandler()),
    /**
     * 经纬度坐标
     */
    GeoPoint(new SimpleParser(), new GeoPointHandler()),
    /**
     * Params{"1223":122345}
     */
    Params(new SimpleParser(), new ParamsHandler()),
    /**
     * Params{"1223":122345}
     */
    Param(new SimpleParser(), new ParamHandler()),
    /**
     * Number dis_max
     */
    NumberDisMax(new SimpleDisMaxParser(), new NumberDisMaxHandler()),
    ;

    private final QueryFieldConfigParser parser;
    private final QueryFieldHandler handler;

    QueryFieldType(QueryFieldConfigParser parser, QueryFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public void handle(QueryField field, SearchRequestContext context, StatementOperator operator,
                       String fieldValue, ListMultimap<Boolean, QueryBuilder> queryBuilders) {
        handler.handle(field.getConfig(), context, field.getFace(), operator, fieldValue, queryBuilders);
    }

    public QueryFieldConfigParser getParser() {
        return parser;
    }

    public QueryFieldHandler getHandler() {
        return handler;
    }

}