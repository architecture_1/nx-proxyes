package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年10月15日
 */
public class FieldValueFactorFunctionParser implements ScoreFunctionFieldConfigParser {

    private static final Pattern FORMAT = Pattern.compile("[\\w.]++");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String fieldName = MoreMaps.getString(settings, "fieldName", "");
        Preconditions.checkState(FORMAT.matcher(fieldName).matches(), "param fieldName empty or incorrect");
        Float weight = MoreMaps.getFloat(settings, "weight", 0f);
        Double missing = MoreMaps.getDouble(settings, "missing", 0d);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("fieldName", fieldName);
        builder.put("weight", weight);
        builder.put("missing", missing);
        return builder.build();
    }

}
