package com.nx.platform.es.biz.esspider.handler;

/**
 * @author
 * @date 2018/04/13
 */
interface Constants {

    String CODE = "code";
    String HANDLERS = "handlers";
    String HANDLERS_ID = "id";
    String HANDLERS_CLASS_NAME = "class-name";

}
