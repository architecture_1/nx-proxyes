package com.nx.platform.es.system.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * 阿波罗配置中心
 */
public class ApolloUtil {

    private static Config apolloConfig = ConfigService.getAppConfig();

    /**
     * getApollo: apollo配置中心
     *
     * @return
     * @author liupengfei
     */
    public static Config getConfig() {
        return apolloConfig;
    }

    /**
     * @program: nx-esproxy-service
     * @description: ${description}
     * @author: Mr.JY
     * @create: 2019-08-30 13:14
     **/
    public static class  GsonUtil {
        private static final Gson GSON = new GsonBuilder().create();

        private static final Type PARAMS_TYPE = new TypeToken<List<Param>>() {
        }.getType();
        private static final Type PARAMS2_TYPE = new TypeToken<List<Param2>>() {
        }.getType();
        private static final Type SERVICES_TYPE = new TypeToken<List<Service>>() {
        }.getType();

        public static <T> T fromJson(String value, Type type) {
            return GSON.fromJson(value, type);
        }

        public static List<Param> toParams(String value) {
            return GSON.fromJson(value, PARAMS_TYPE);
        }

        public static List<Param2> toParams2(String value) {
            return GSON.fromJson(value, PARAMS2_TYPE);
        }

        public static List<Service> toServices(String value) {
            return GSON.fromJson(value, SERVICES_TYPE);
        }

        public static class Param {

            private Integer paramId;
            private String valueId;

            public Integer getParamId() {
                return paramId;
            }

            public String getValueId() {
                return valueId;
            }

            public void setParamId(Integer paramId) {
                this.paramId = paramId;
            }

            public void setValueId(String valueId) {
                this.valueId = valueId;
            }
        }

        public static class Param2 {

            private Integer pId;
            private String vId;

            public Integer getpId() {
                return pId;
            }

            public String getvId() {
                return vId;
            }

            public void setpId(Integer pId) {
                this.pId = pId;
            }

            public void setvId(String vId) {
                this.vId = vId;
            }
        }

        public static class Service {

            private Long serviceId;

            public Long getServiceId() {
                return serviceId;
            }

            public void setServiceId(Long serviceId) {
                this.serviceId = serviceId;
            }

        }
    }
}
