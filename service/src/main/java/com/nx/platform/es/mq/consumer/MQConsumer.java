package com.nx.platform.es.mq.consumer;

import com.alibaba.rocketmq.client.consumer.DefaultMQPushConsumer;
import com.nx.platform.es.system.init.SystemInit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * MQ客户端
 */
@Component
public class MQConsumer {
    private static final Logger logger = LoggerFactory.getLogger(MQConsumer.class);
    /**
     * 消费者配置文件路径
     */
    private static String CONSUMER_CONFIG = SystemInit.CONFIG_PATH + "/mq_consumer.config";

    @Autowired
    private MQMessageListener mqMessageListener;

    /**
     * 启动客户端
     */
    @PostConstruct
    public void start() {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("dealESConsumer"); //应该从配置中取
        consumer.registerMessageListener(mqMessageListener);
//        try {
//           暂无mq需求，esmq单独处理
//            consumer.init(CONSUMER_CONFIG);
//            consumer.start();
//            logger.info("MQConsumer init ok.");
//        } catch (IOException | MQClientException e) {
//            logger.error("desc=consumeMessage, ex=" + E2s.exception2String(e));
//        }
    }

}
