package com.nx.platform.es.system.filter;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 依赖rpc框架的代码，暂时注释
 */
@Slf4j
public class RequestFilter {
//public class RequestFilter implements IFilter {

//    @Override
//    public void filter(SCFContext scfContext) throws Exception {
//        try {
//            RequestProtocol request = (RequestProtocol) scfContext.getScfRequest().getProtocol().getSdpEntity();
//            String methodName = request.getMethodName();
//            String lookUp = request.getLookup();
//            scfContext.getStopWatch().startNew("naixue", lookUp + "::" + methodName);
//
//            //获取logstr logid
//            List<KeyValuePair> params = request.getParaKVList();
//            StringBuilder logStr = new StringBuilder();
//            for (int i = 0; i < params.size(); i++) {
//                KeyValuePair param = params.get(i);
//                Object value = param.getValue();
//                if (value instanceof Map) {
//                    Map<String, String> p = (Map<String, String>) value;
//                    logStr.append(p.get("logStr"));
//                    break;
//                } else {
//                    String tmp = JSON.toJSONString(value);
//                    if (tmp.toLowerCase().contains("logid")) {
//                        logStr.append(" ").append(tmp.replaceAll("\"", "").replaceAll("\\\\u003d", "="));
//                    }
//                }
//            }
//
//            Matcher logid = Pattern.compile("logid=[\\d]+").matcher(logStr.toString());
//            if (logid.find()) {
//                //log4j
//                MDC.put("logStr",logid.group());
//            }
//
//            logStr.append(lookUp).append("::").append(methodName);
//
//            //输出调用方的ip和端口,callerkey和序列化版本
//            String requestIp = scfContext.getChannel().getRemoteIP();
//            int requestPort = scfContext.getChannel().getRemotePort();
//            String callerName = scfContext.getCallerName();
//            log.info("{} ip={} port={} callerName={} desc=start", logStr.toString(), requestIp, requestPort, callerName);
//
//            StopWatch.PerformanceCounter couter = scfContext.getStopWatch().new PerformanceCounter();
//            couter.setKey("dealES_filter");
//            couter.setDescription(logStr.toString());
//            couter.setStartTime(System.currentTimeMillis());
//            scfContext.getStopWatch().getMapCounter().put("dealES_filter", couter);

//        } catch (Throwable e) {
//            log.error("desc=RequestFilter.filter", e);
//        }
//    }

//    @Override
//    public int getPriority() {
//        return 0;
//    }
}
