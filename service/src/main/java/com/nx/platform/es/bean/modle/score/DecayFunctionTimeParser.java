package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;
import org.elasticsearch.common.unit.TimeValue;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class DecayFunctionTimeParser implements ScoreFunctionFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String fieldName = MoreMaps.getString(settings, "fieldName");
        Preconditions.checkState(!Strings.isNullOrEmpty(fieldName), "param fieldName not found");
        String scale = MoreMaps.getString(settings, "scale", "").trim();
        validateTimeString("scale", scale);
        String offset = MoreMaps.getString(settings, "offset", "").trim();
        validateTimeString("offset", offset);
        double decay = MoreMaps.getDoubleValue(settings, "decay", 0.5d);
        float weight = MoreMaps.getFloatValue(settings, "weight", 0f);
        int precision = MoreMaps.getIntValue(settings, "precision", 0);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("fieldName", fieldName);
        builder.put("scale", scale);
        builder.put("offset", offset);
        builder.put("decay", decay);
        builder.put("weight", weight);
        builder.put("precision", precision);
        return builder.build();
    }

    private void validateTimeString(String name, String value) {
        TimeValue.parseTimeValue(value, name);
    }

}
