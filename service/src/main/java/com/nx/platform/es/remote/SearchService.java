package com.nx.platform.es.remote;


import com.nx.platform.es.biz.query.search.SearchRequestContext;
import com.nx.platform.es.biz.query.search.SearchRequestProcessor;
import com.nx.platform.es.contract.ISearchService;
import com.nx.platform.es.entity.response.SearchResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author alex
 * @brief
 * @date 2019/11/4
 */
@Slf4j
public class SearchService implements ISearchService {
    @Autowired
    private SearchRequestProcessor searchRequestProcessor;

    @Override
    public SearchResult search(String logStr, String index, String query, String params) throws Exception {
        SearchRequestContext context = new SearchRequestContext(logStr, index, query, params);
        log.info("{} desc=search index={} query={} param={}", logStr, index, query, params);
        return searchRequestProcessor.request(context);
    }

    @Override
    public SearchResult searchWithFields(String logStr, String index, String query, String params) throws Exception {
        SearchRequestContext context = new SearchRequestContext(logStr, index, query, params);
        context.setFetchFields(true);
        return searchRequestProcessor.request(context);
    }
}
