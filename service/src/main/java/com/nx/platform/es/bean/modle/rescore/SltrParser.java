package com.nx.platform.es.bean.modle.rescore;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreSplitters;
import com.nx.platform.es.common.utils.MoreMaps;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Map;
import java.util.Set;

/**
 * @author
 * @since 2016年10月15日
 */
public class SltrParser implements RescoreFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        int windowSize = MoreMaps.getIntValue(settings, "window_size", 0);
        float queryWeight = MoreMaps.getFloatValue(settings, "query_weight", 1f);
        float rescoreQueryWeight = MoreMaps.getFloatValue(settings, "rescore_query_weight", 1f);
        String model = MoreMaps.getString(settings, "model");
        Preconditions.checkState(!Strings.isNullOrEmpty(model), "param model not found");
        Set<String> paramNames = MoreMaps.getStringSet(settings, "paramNames", MoreSplitters.COMMA);
        Preconditions.checkState(CollectionUtils.isNotEmpty(paramNames), "param paramNames not found or empty");
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("window_size", windowSize);
        builder.put("query_weight", queryWeight);
        builder.put("rescore_query_weight", rescoreQueryWeight);
        builder.put("model", model);
        builder.put("paramNames", paramNames);
        return builder.build();
    }

}
