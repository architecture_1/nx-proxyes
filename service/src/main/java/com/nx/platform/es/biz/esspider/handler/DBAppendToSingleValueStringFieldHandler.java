package com.nx.platform.es.biz.esspider.handler;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.nx.platform.es.biz.esspider.resource.DBManager;
import com.nx.platform.es.biz.esspider.entity.Item;
import com.nx.platform.es.common.utils.MoreMaps;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 从数据库中获取内容，并追加到单值的字符串类型的字段
 *
 * @author
 * @since 2017年3月24日
 */
@HandlerDefine(HandlerType.DB)
public class DBAppendToSingleValueStringFieldHandler extends AbstractDBHandler {

    private String sql;
    private String idField;
    private String oriField; // 查询结果中字段名
    private String field; // 追加到文档字段名
    private boolean toString;

    public DBAppendToSingleValueStringFieldHandler(String identity, DBManager dbManager) {
        super(identity, dbManager);
    }

    @Override
    public void init(Map<?, ?> settings) throws Exception {
        sql = MoreMaps.getString(settings, "sql");
        idField = MoreMaps.getString(settings, "idField");
        oriField = MoreMaps.getString(settings, "oriField");
        field = MoreMaps.getString(settings, "field");
        toString = MoreMaps.getBooleanValue(settings, "toString", false);
        Preconditions.checkNotNull(getJdbcTemplate());
        Preconditions.checkArgument(!Strings.isNullOrEmpty(sql), "sql null or empty");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(idField), "idField null or empty");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(oriField), "oriField null or empty");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(field), "field null or empty");
    }

    @Override
    public void handle(Map<Long, Item> items) throws Exception {
        if (items.isEmpty()) {
            return;
        }
        // 查询数据库
        SqlParameterSource queryParams;
        if (toString) {
            queryParams = new MapSqlParameterSource(SQL_IDS_KEY,
                    items.keySet().stream().map(String::valueOf).collect(Collectors.toList()));
        } else {
            queryParams = new MapSqlParameterSource(SQL_IDS_KEY, items.keySet());
        }
        List<Map<String, Object>> queryResults = getJdbcTemplate().queryForList(sql, queryParams);
        if (queryResults.isEmpty()) {
            return;
        }
        // 更新文档
        for (Map<String, Object> queryResult : queryResults) {
            Long id = MoreMaps.getLong(queryResult, idField);
            if (id == null) {
                continue;
            }
            Item item = items.get(id);
            if (item == null) {
                continue;
            }
            String fieldValue = MoreMaps.getString(queryResult, field, "");
            String oriFieldValue = MoreMaps.getString(item.getDoc(), oriField, "");
            item.getDoc().put(oriField, oriFieldValue + " " + fieldValue);
        }
    }

}
