package com.nx.platform.es.mq.producer;

import com.alibaba.rocketmq.client.producer.SendCallback;
import com.alibaba.rocketmq.client.producer.SendResult;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MQCallback implements SendCallback {
    private Long orderId;
    private String tag;
    private String logStr;

    public MQCallback(Long orderId, String tag, String logStr) {
        this.orderId = orderId;
        this.tag = tag;
        this.logStr = logStr;
    }

    @Override
    public void onException(Throwable throwable) {
        log.error("{} desc={} retsult=fail orderId={}",logStr, tag, orderId);
    }

    @Override
    public void onSuccess(SendResult sendresult) {
        log.info("{} desc={} orderId={} result=success", logStr, tag, orderId);
    }

}
