package com.nx.platform.es.service.impl;


import com.nx.platform.es.biz.query.agg.AggRequestProcessor;
import com.nx.platform.es.biz.query.agg.cache.AggCache;
import com.nx.platform.es.biz.query.agg.cache.AggCaches;
import com.nx.platform.es.biz.query.agg.cache.CodisAggCache;
import com.nx.platform.es.biz.query.agg.handler.*;
import com.nx.platform.es.biz.query.count.CountRequestProcessor;
import com.nx.platform.es.biz.query.count.cache.CodisCountCache;
import com.nx.platform.es.biz.query.count.cache.CountCache;
import com.nx.platform.es.biz.query.count.cache.CountCaches;
import com.nx.platform.es.biz.query.count.handler.*;
import com.nx.platform.es.biz.query.search.SearchRequestProcessor;
import com.nx.platform.es.biz.query.search.cache.CodisSearchCache;
import com.nx.platform.es.biz.query.search.cache.SearchCache;
import com.nx.platform.es.biz.query.search.cache.SearchCaches;
import com.nx.platform.es.biz.query.search.handler.*;
import com.nx.platform.es.service.ESClientManager;
import com.nx.platform.es.system.config.ESQueryConfigure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author alex
 * @brief
 * @date 2019/11/11
 */
@Component
public class ClientBeans {

    @Bean
    public ESClientManager esClientManager() {
        ESClientManagerImpl esClientManager = new ESClientManagerImpl();
        esClientManager.startAsync().awaitRunning();
        return esClientManager;
    }

    @Autowired
    ESQueryConfigure configs;

    @Bean
    public CodisSearchCache codisSearchCache() {
        return new CodisSearchCache();
    }

    @Bean
    public CodisAggCache codisAggCache() {
        return new CodisAggCache();
    }

    @Bean
    public CodisCountCache codisCountCache() {
        return new CodisCountCache();
    }

    @Bean
    @Autowired
    public SearchRequestProcessor searchRequestProcessor(
            ESClientManager clients, ESQueryConfigure configs,
            SearchCache codisSearchCache) {
        SearchRequestHandlerChain chain = new SearchRequestHandlerChainImpl(clients, configs);
        chain.appendHandler(SearchSortHandler.INSTANCE);
        chain.appendHandler(SearchIndexHandler.INSTANCE);
        chain.appendHandler(SearchPageHandler.INSTANCE);
        chain.appendHandler(SearchQueryHandler.INSTANCE);
        chain.appendHandler(SearchCacheHandler.INSTANCE);
        SearchCaches cache = new SearchCaches(codisSearchCache);
        return new SearchRequestProcessor(chain, cache);
    }

    @Bean
    @Autowired
    public AggRequestProcessor aggRequestProcessor(
            ESClientManager clients, ESQueryConfigure configs,
            AggCache codisAggCache) {
        AggRequestHandlerChain chain = new AggRequestHandlerChainImpl(clients, configs);
        chain.appendHandler(AggIndexHandler.INSTANCE);
        chain.appendHandler(AggPageHandler.INSTANCE);
        chain.appendHandler(AggQueryHandler.INSTANCE);
        chain.appendHandler(AggHandler.INSTANCE);
        chain.appendHandler(AggCacheHandler.INSTANCE);
        AggCaches cache = new AggCaches(codisAggCache);
        return new AggRequestProcessor(chain, cache);
    }

    @Bean
    @Autowired
    public CountRequestProcessor countRequestProcessor(
            ESClientManager clients, ESQueryConfigure configs,
            CountCache codisCountCache) {
        CountRequestHandlerChain chain = new CountRequestHandlerChainImpl(clients, configs);
        chain.appendHandler(CountIndexHandler.INSTANCE);
        chain.appendHandler(CountPageHandler.INSTANCE);
        chain.appendHandler(CountQueryHandler.INSTANCE);
        chain.appendHandler(CountCacheHandler.INSTANCE);
        CountCaches cache = new CountCaches(codisCountCache);
        return new CountRequestProcessor(chain, cache);
    }
}
