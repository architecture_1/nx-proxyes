package com.nx.platform.es.bean.modle.score;

import com.google.common.collect.ImmutableMap;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface ScoreFunctionFieldHandler {

    /**
     * @param fieldConfig
     * @param params
     * @return
     */
    FilterFunctionBuilder handle(ImmutableMap<String, ?> fieldConfig, Map<String, Object> params);

}
