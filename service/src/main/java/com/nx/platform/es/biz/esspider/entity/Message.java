package com.nx.platform.es.biz.esspider.entity;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 中间对象，用于对接原始信息和最终的doc信息
 * @author
 * @since 2017年3月24日
 */
public class Message {

    private String source; // 来源
    private LocalDateTime timestamp; // 时间
    private String type; // 类型
    private Long id; // ID
    private Map<String, Object> doc; // 属性
    private String line; // 原始行

    public Message() {}

    public Message(Class<?> source, String type, Long id, Map<String, Object> doc, String line) {
        this.source = source != null ? source.getSimpleName() : null;
        this.timestamp = LocalDateTime.now();
        this.type = type;
        this.id = id;
        this.doc = doc;
        this.line = line;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Message [source=");
        builder.append(source);
        builder.append(", timestamp=");
        builder.append(timestamp);
        builder.append(", type=");
        builder.append(type);
        builder.append(", id=");
        builder.append(id);
        builder.append(", doc=");
        builder.append(doc);
        builder.append(", line=");
        builder.append(line);
        builder.append("]");
        return builder.toString();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, Object> getDoc() {
        return doc;
    }

    public void setDoc(Map<String, Object> doc) {
        this.doc = doc;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

}
