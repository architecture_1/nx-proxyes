package com.nx.platform.es.bean.modle.query;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ListMultimap;
import com.nx.platform.es.bean.dto.RequestContext;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import org.elasticsearch.index.query.QueryBuilder;

/**
 * @author
 * @since 2016年10月13日
 */
public interface QueryFieldHandler {

    void handle(ImmutableMap<String, ?> fieldConfig, RequestContext context, String fieldFace,
                StatementOperator operator, String fieldValue, ListMultimap<Boolean, QueryBuilder> queryBuilders);

}
