package com.nx.platform.es.bean.modle.query;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface QueryFieldConfigParser {

    ImmutableMap<String, ?> parse(Map<?, ?> settings);

}
