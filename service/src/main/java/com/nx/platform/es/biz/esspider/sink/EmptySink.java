package com.nx.platform.es.biz.esspider.sink;

import com.nx.platform.es.biz.esspider.entity.Item;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;

/**
 * @author
 * @date 2018/01/28
 */
@SinkDefine(SinkType.Simple)
public class EmptySink extends AbstractSimpleSink {

    public EmptySink(String identity) {
        super(identity);
    }

    @Override
    public @NotNull Map<Long, Item> accept(Map<Long, Item> items) {
        return Collections.emptyMap();
    }

}
