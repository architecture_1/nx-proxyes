package com.nx.platform.es.bean.modle.rescore;

/**
 * @author
 * @since 2016年10月15日
 */
public enum RescoreFieldType {

    ScoreFunctions(new SimpleParser(), new FunctionScoresHandler()),
    Sltr(new SltrParser(), new SltrHandler());

    private final RescoreFieldConfigParser parser;
    private final RescoreFieldHandler handler;

    RescoreFieldType(RescoreFieldConfigParser parser, RescoreFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public RescoreFieldConfigParser getParser() {
        return parser;
    }

    public RescoreFieldHandler getHandler() {
        return handler;
    }

}
