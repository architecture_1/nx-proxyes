package com.nx.platform.es.biz.esspider.handler;

import com.nx.platform.es.biz.esspider.entity.Item;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * @author
 * @since 2017年3月27日
 */
@HandlerDefine(HandlerType.SIMPLE)
public class SimpleInfoFactorScoreHandler extends AbstractSimpleHandler {

    public static final String TIMESTAMP = "timestamp";
    public static final String PICS = "pics";
    public static final String CONTENT_LENGTH = "content_length";
    public static final String LABEL = "label";
    public static final String ORI_PRICE = "ori_price";
    public static final String VILLAGE = "village";
    public static final String FREIGHT = "freight";
    public static final String UV_SCORE = "uv_score";
    public static final String PV_SCORE = "pv_score";
    public static final String WANT_BUY_COUNT_SCORE = "want_buy_count_score";
    public static final String COMMENT_COUNT_SCORE = "comment_count_score";
    public static final String CITY = "city";

    public static final String BUYER_PRAISE_RATE_SCORE = "buyer_praise_rate_score";
    public static final String VENDOR_SCORE = "vendor_socore";
    public static final String SELLER_ACTIVE_SCORE = "seller_active_score";
    public static final String RAIN_SCORE = "seller_active_score";
    public static final String COORDS = "coords";
    public static final String SEX_IDENTIFY = "sex_identify";
    public static final String CLEAR_TREND = "clear_trend";
    public static final String ACTION_TREND = "action_trend";
    public static final String FUZZY_TREND = "fuzzy_trend";
    public static final String CATE_PARENT_ID = "cate_parent_id";
    public static final String CATE_ID = "cate_id";
    public static final String ADJUST_SCORE = "adjust_score";

    @Override
    public void handle(Map<Long, Item> items) throws Exception {
        items.values().forEach((Item item) -> {
            Map<String, Object> doc = item.getDoc();
            doc.put("factor_score_infobase", factorScoreInfobase(doc));
            doc.put("factor_score_userworth", factorScoreUserworth(doc));
            doc.put("content_length", contentLength(doc));
            doc.put("discount", discount(doc));
        });
    }

    private float discount(Map<String, Object> params) {
        int firstPrice = MapUtils.getIntValue(params, "first_price", 0);
        int nowPrice = MapUtils.getIntValue(params, "now_price", 0);
        int oriPrice = MapUtils.getInteger(params, "ori_price", 0);
        // 折扣计算，有原价时 100 * 现价 / 原价，无原价时， 100 * 现价 / 初始价格
        if (oriPrice <= 0) {
            if (firstPrice <= 0) {
                return 100f;
            } else {
                return (float) (100d * nowPrice / firstPrice);
            }
        } else {
            return (float) (100d * nowPrice / oriPrice);
        }
    }

    private int contentLength(Map<String, Object> params) {
        Object title = params.get("title");
        Object content = params.get("content");
        return length(title) + length(content);
    }

    private int length(Object obj) {
        if (obj == null) {
            return 0;
        }
        return String.valueOf(obj).length();
    }

    private float factorScoreInfobase(Map<String, Object> params) {
        float score = 0f;
        // 1.2 信息质量分
        // 信息填写的完整度(总分2分)
        if (getInt(params, PICS, 0) > 3) {// 图片大于3张+0.5
            score += 0.5;
        }
        // 标题加描述，目前暂时存放在一个字段中
        if (getInt(params, CONTENT_LENGTH, 0) >= 50) {// 标题+描述超过50字+0.5
            score += 0.5f;
        }
        if (getInt(params, LABEL, 0) > 0) {// 勾选了标签
            score += 0.5f;
        }
        if (getInt(params, ORI_PRICE, 0) > 0 || getInt(params, FREIGHT, 0) > 0) {// 填写了原价或者运费
            score += 0.5f;
        }
        // 1.3 信息得到的反馈数(总分3分)
        score += getFloat(params, PV_SCORE, 0.0f);
        score += getFloat(params, WANT_BUY_COUNT_SCORE, 0.0f);
        return score;
    }

    private float factorScoreUserworth(Map<String, Object> params) {
        float score = 0f;
        // 卖家有好评
        score += getFloat(params, BUYER_PRAISE_RATE_SCORE, 0.0f);
        // 不是疑似商家
        score += getFloat(params, VENDOR_SCORE, 0.0f);
        // 活跃程度总分
        score += getFloat(params, SELLER_ACTIVE_SCORE, 0.0f);
        // 卖家RAIN分
        float rainScore = getFloat(params, RAIN_SCORE, 0.0f);
        // if (rainScore <= 1) {
        // score += 3.0;
        // } else if ((rainScore >= 100)) {
        // score += 0.0;
        // } else {
        // score += -(1d / 33) * rainScore + 100d / 33;
        // }
        score += rainScore;
        return score;
    }

    private int getInt(Map<String, Object> params, String key, int defaultValue) {
        Object value = params.get(key);
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Number) {
            return ((Number) value).intValue();
        } else if (value instanceof Boolean) {
            return ((Boolean) value) ? 1 : 0;
        } else {
            return defaultValue;
        }
    }

    private float getFloat(Map<String, Object> params, String key, float defaultValue) {
        Object value = params.get(key);
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Number) {
            return ((Number) value).floatValue();
        } else if (value instanceof Boolean) {
            return ((Boolean) value) ? 1f : 0f;
        } else {
            return defaultValue;
        }
    }

}
