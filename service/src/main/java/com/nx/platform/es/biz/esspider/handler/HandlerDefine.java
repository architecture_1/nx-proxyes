package com.nx.platform.es.biz.esspider.handler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据过滤处理器
 * 
 * @author
 * @since 2017年3月22日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HandlerDefine {

    /**
     * 过滤处理器类型
     * 
     * @return
     */
    HandlerType value();

}
