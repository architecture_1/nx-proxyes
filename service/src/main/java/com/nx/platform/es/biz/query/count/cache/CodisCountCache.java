package com.nx.platform.es.biz.query.count.cache;

import com.google.common.base.Strings;
import com.google.common.primitives.Longs;
import com.nx.arch.redis.clients.jedis.Jedis;
import com.nx.platform.es.system.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CodisCountCache implements CountCache {


    @Override
    public Long get(String logStr, String cacheKey) {
        Long result = null;
        String redisKey = CACHE_KAY_PREFIX + cacheKey;
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getRedis();
            if (jedis == null) {
                log.error("{} method=get redisKey={} e=jedis is null", logStr, redisKey);
                return null;
            }
            String value = jedis.get(redisKey);
            if (!Strings.isNullOrEmpty(value)) {
                result = Longs.tryParse(value);
            }
        } catch (Exception e) {
            log.error("{} method=get redisKey={} e=", logStr, redisKey, e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    @Override
    public void put(String logStr, String cacheKey, Long result, int seconds) {
        String redisKey = CACHE_KAY_PREFIX + cacheKey;
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getRedis();
            if (jedis == null) {
                log.error("{} method=put redisKey={} e=jedis is null", logStr, redisKey);
                return;
            }
            jedis.setex(redisKey, seconds > 0 ? seconds : 60, String.valueOf(result));
        } catch (Exception e) {
            log.error("{} method=put redisKey={} e=", logStr, redisKey, e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

}
