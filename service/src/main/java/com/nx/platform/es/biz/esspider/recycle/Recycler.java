package com.nx.platform.es.biz.esspider.recycle;

import com.nx.platform.es.biz.esspider.entity.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @author
 * @date 2018/04/13
 */
public interface Recycler {

    Logger LOGGER = LogManager.getLogger(Recycler.class);

    /**
     * 处理失败的items存储，后续回收
     *
     * @param items
     */
    void recycle(Map<Long, Item> items);

    /**
     * 回收存储的数据
     *
     * @param consumer
     */
    void recycle(Consumer<String> consumer);

    /**
     * 打开回收站
     *
     * @throws Exception
     */
    void open() throws Exception;

    /**
     * 关闭回收站
     *
     * @throws Exception
     */
    void close() throws Exception;

}
