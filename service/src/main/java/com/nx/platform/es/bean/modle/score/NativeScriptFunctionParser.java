package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class NativeScriptFunctionParser implements ScoreFunctionFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String scriptName = MoreMaps.getString(settings, "scriptName");
        Preconditions.checkState(!Strings.isNullOrEmpty(scriptName), "param scriptName not found");
        Float weight = MoreMaps.getFloat(settings, "weight", 0f);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("scriptName", scriptName);
        builder.put("weight", weight);
        return builder.build();
    }

}
