package com.nx.platform.es.biz.query.agg.handler;

import com.nx.platform.es.biz.query.agg.AggRequestContext;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchType;

/**
 * @author
 * @since 2016年10月15日
 */
public enum AggIndexHandler implements AggRequestHandler {

    INSTANCE;

    @Override
    public void handle(AggRequestContext context) {
        SearchRequest request = context.getRequest();
        request.indices(context.getIndex()).types(context.getType());
        request.searchType(SearchType.QUERY_THEN_FETCH);
        context.getTrace().append("&_entry=").append(context.getEntry());
    }

}
