package com.nx.platform.es.biz.query.count.cache;

/**
 * @author
 * @date 2018/02/28
 */
public class NulllCountCache implements CountCache {

    @Override
    public Long get(String logStr, String cacheKey) {
        return null;
    }

    @Override
    public void put(String logStr, String cacheKey, Long result, int seconds) {
        // empty ok
    }

}
