package com.nx.platform.es.mq.constants;

import com.nx.platform.es.system.init.SystemInit;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 从生产者配置文件提取MQTopic配置
 */
public class MQTopics {
    public static String PRODUCER_CONFIG = SystemInit.CONFIG_PATH + "/mq_producer.config";
    private static String tradeTopic;

    static {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(PRODUCER_CONFIG));
            tradeTopic = properties.getProperty("trade_Topic");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getTradeTopic() {
        return tradeTopic;
    }

}
