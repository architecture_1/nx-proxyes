package com.nx.platform.es.common.utils;

import org.apache.commons.lang3.StringUtils;


/**
 * @author
 * @since Dec 30, 2016
 */
public class PathUtil {

    public static String getConfigPath() {
        String path = System.getProperty("config_path");
        if (StringUtils.isBlank(path)) {
//            return Path.getCurrentPath() + "/config";
            return "./config";
        } else {
            return path;
        }
    }

    public static void setConfigPath(String path) {
        if (StringUtils.isNotBlank(path)) {
            System.setProperty("config_path", path);
        }
    }

}
