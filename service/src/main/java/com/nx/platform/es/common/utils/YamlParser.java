package com.nx.platform.es.common.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @since Apr 5, 2016
 */
public class YamlParser {

    private static final Logger LOGGER = LogManager.getLogger(YamlParser.class);
    private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

    @NotNull
    public static Map<?, ?> parseToMap(File file) {
        LOGGER.info("file({}) parsing ...", file.getAbsolutePath());
        Reader reader = null;
        try {
            reader = new FileReader(file);
            Map<?, ?> original = new Yaml().loadAs(reader, Map.class);
            LOGGER.info("file({}) parsed\n{}", file.getAbsolutePath(), GSON.toJson(original));
            return original == null ? Collections.emptyMap() : original;
        } catch (Exception e) {
            LOGGER.error("file({}) parse error.", file.getAbsolutePath(), e);
            return Collections.emptyMap();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("file({}) close error.", file.getAbsolutePath(), e);
                }
            }
        }
    }

    @NotNull
    @SuppressWarnings("unchecked")
    public static <T> List<T> parseToList(File file) {
        LOGGER.info("file({}) parsing ...", file.getAbsolutePath());
        Reader reader = null;
        try {
            reader = new FileReader(file);
            List<T> original = new Yaml().loadAs(reader, List.class);
            LOGGER.info("file({}) parsed\n{}", file.getAbsolutePath(), GSON.toJson(original));
            return original == null ? Collections.emptyList() : original;
        } catch (Exception e) {
            LOGGER.error("file({}) parse error.", file.getAbsolutePath(), e);
            return Collections.emptyList();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("file({}) close error.", file.getAbsolutePath(), e);
                }
            }
        }
    }

    @NotNull
    public static Map<?, ?> parseToMap(String str) {
        try {
            Map<?, ?> result = new Yaml().loadAs(str, Map.class);
            return result == null ? Collections.emptyMap() : result;
        } catch (Exception e) {
            LOGGER.error(e);
            return Collections.emptyMap();
        }
    }

    @NotNull
    @SuppressWarnings("unchecked")
    public static <T> List<T> parseToList(String str) {
        try {
            List<T> result = new Yaml().loadAs(str, List.class);
            return result == null ? Collections.emptyList() : result;
        } catch (Exception e) {
            LOGGER.error(e);
            return Collections.emptyList();
        }
    }

}
