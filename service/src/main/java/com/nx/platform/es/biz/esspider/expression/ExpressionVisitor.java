// Generated from src/main/resources/META-INF/Expression.g4 by ANTLR 4.7
package com.nx.platform.es.biz.esspider.expression;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExpressionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExpressionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code compare}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompare(ExpressionParser.CompareContext ctx);
	/**
	 * Visit a parse tree produced by the {@code not}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(ExpressionParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code or}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(ExpressionParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code in}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIn(ExpressionParser.InContext ctx);
	/**
	 * Visit a parse tree produced by the {@code and}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(ExpressionParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parensExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParensExpr(ExpressionParser.ParensExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exsit}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExsit(ExpressionParser.ExsitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code number}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(ExpressionParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitAndOr}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitAndOr(ExpressionParser.BitAndOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addSub}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddSub(ExpressionParser.AddSubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code params}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParams(ExpressionParser.ParamsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parensVal}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParensVal(ExpressionParser.ParensValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code muldiv}
	 * labeled alternative in {@link ExpressionParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMuldiv(ExpressionParser.MuldivContext ctx);
}