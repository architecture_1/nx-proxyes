package com.nx.platform.es.biz.query.search.cache;

import com.google.common.base.Strings;
import com.nx.arch.redis.clients.jedis.Jedis;
import com.nx.platform.es.entity.response.SearchResult;
import com.nx.platform.es.system.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CodisSearchCache implements SearchCache {

    @Override
    public SearchResult get(String logStr, String cacheKey) {
        SearchResult result = null;
        String redisKey = CACHE_KAY_PREFIX + cacheKey;
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getRedis();
            if (jedis == null) {
                log.error("{} method=get redisKey={} e=jedis is null", logStr, redisKey);
                return null;
            }
            String value = jedis.get(redisKey);
            if (!Strings.isNullOrEmpty(value)) {
                result = GSON.fromJson(value, SearchResult.class);
            }
        } catch (Exception e) {
            log.error("{} method=get redisKey={} e=", logStr, redisKey, e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    @Override
    public void put(String logStr, String cacheKey, SearchResult result, int seconds) {
        String redisKey = CACHE_KAY_PREFIX + cacheKey;
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getRedis();
            if (jedis == null) {
                log.error("{} method=put redisKey={} e=jedis is null", logStr, redisKey);
                return;
            }
            jedis.setex(redisKey, seconds > 0 ? seconds : 60, GSON.toJson(result));
        } catch (Exception e) {
            log.error("{} method=put redisKey={} e=", logStr, redisKey, e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

}
