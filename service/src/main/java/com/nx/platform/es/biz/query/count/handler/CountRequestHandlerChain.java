package com.nx.platform.es.biz.query.count.handler;

/**
 * 请求处理链
 * <p>
 * Created by  on 2017/4/18.
 */
public interface CountRequestHandlerChain extends CountRequestHandler {

    void appendHandler(CountRequestHandler handler);

}
