package com.nx.platform.es.bean.modle.score;

/**
 * @author
 * @since 2016年10月15日
 */
public enum ScoreFunctionFieldType {
    /***/
    FieldValueFactorFunction(new FieldValueFactorFunctionParser(), new FieldValueFactorFunctionHandler()),
    /***/
    GaussDecayFunctionGeoPoint(new DecayFunctionGeoPointParser(), new GaussDecayFunctionGeoPointHandler()),
    /***/
    GaussDecayFunctionTime(new DecayFunctionTimeParser(), new GaussDecayFunctionTimeHandler()),
    /***/
    LineDecayFunctionGeoPoint(new DecayFunctionGeoPointParser(), new LineDecayFunctionGeoPointHandler()),
    /***/
    LineDecayFunctionTime(new DecayFunctionTimeParser(), new LineDecayFunctionTimeHandler()),
    /***/
    ExpDecayFunctionGeoPoint(new DecayFunctionGeoPointParser(), new ExpDecayFunctionGeoPointHandler()),
    /***/
    ExpDecayFunctionTime(new DecayFunctionTimeParser(), new ExpDecayFunctionTimeHandler()),
    /***/
    NativeScriptFunction(new NativeScriptFunctionParser(), new NativeScriptFunctionHandler()),
    /***/
    PainlessScriptFunction(new PainlessScriptFunctionParser(), new PainlessScriptFunctionHandler()),
    /***/
    GeoDistanceFilter(new GeoDistanceFilterParser(), new GeoDistanceFilterHandler()),
    /***/
    NumberTermsFilter(new NumberTermsFilterParser(), new NumberTermsFilterHandler()),
    /***/
    ReverseNumberTermsFilter(new NumberTermsFilterParser(), new ReverseNumberTermsFilterHandler()),
    /***/
    ParamNumberTermsFilter(new ParamNumberTermsFilterParser(), new ParamNumberTermsFilterHandler()),
    /***/
    ReverseParamNumberTermsFilter(new ParamNumberTermsFilterParser(), new ReverseParamNumberTermsFilterHandler());

    private final ScoreFunctionFieldConfigParser parser;
    private final ScoreFunctionFieldHandler handler;

    ScoreFunctionFieldType(ScoreFunctionFieldConfigParser parser, ScoreFunctionFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public ScoreFunctionFieldConfigParser getParser() {
        return parser;
    }

    public ScoreFunctionFieldHandler getHandler() {
        return handler;
    }

}
