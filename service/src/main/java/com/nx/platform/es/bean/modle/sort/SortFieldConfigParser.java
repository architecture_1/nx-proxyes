package com.nx.platform.es.bean.modle.sort;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface SortFieldConfigParser {

    /**
     * @param map
     * @return
     */
    ImmutableMap<String, ?> parse(Map<?, ?> settings);

}
