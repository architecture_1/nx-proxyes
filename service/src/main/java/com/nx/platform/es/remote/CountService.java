package com.nx.platform.es.remote;


import com.nx.platform.es.biz.query.count.CountRequestContext;
import com.nx.platform.es.biz.query.count.CountRequestProcessor;
import com.nx.platform.es.contract.ICountService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author alex
 * @brief
 * @date 2019/11/4
 */
public class CountService implements ICountService {

    @Autowired
    private CountRequestProcessor countRequestProcessor;

    @Override
    public long count(String logStr, String index, String query) throws Exception {
        CountRequestContext context = new CountRequestContext(logStr, index, query, null);
        return countRequestProcessor.request(context);
    }
}
