package com.nx.platform.es.biz.esspider.handler;

import com.nx.platform.es.biz.esspider.entity.Item;

import java.util.Map;

/**
 * Created by  on 2017/7/13.
 */
@HandlerDefine(HandlerType.SIMPLE)
public class EmptyHandler extends AbstractSimpleHandler {


    @Override
    public void handle(Map<Long, Item> items) throws Exception {
        // do nothing
    }

}
