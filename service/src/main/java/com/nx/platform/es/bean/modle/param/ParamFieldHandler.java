package com.nx.platform.es.bean.modle.param;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Table;
import com.nx.platform.es.bean.dto.RequestContext;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface ParamFieldHandler {

    void handle(RequestContext context, ImmutableMap<String, ?> fieldConfig,
                Table<String, StatementOperator, List<String>> queryTable,
                Table<String, StatementOperator, List<String>> paramsTable,
                Map<String, Object> paramsMap);

}
