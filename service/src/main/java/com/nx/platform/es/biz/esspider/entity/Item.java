package com.nx.platform.es.biz.esspider.entity;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * ES 上下文信息，doc 用于刷es
 * @author
 * @since 2017年3月23日
 */
public class Item {

    // sink时生成的文档
    private final ListMultimap<String, Map<String, Object>> effectDocs = LinkedListMultimap.create();
    // 处理结果
    private final ListMultimap<String, String> results = LinkedListMultimap.create();
    // 接收时间(多个消息的取最后一个消息的时间)
    private List<LocalDateTime> timestamp;
    // 消息来源(可能来源于ESB/MQ/File...)
    private List<String> source;
    // 原始消息(同类型的多个消息可能有相同的记录ID)
    private List<String> lines;
    // 记录类型
    private String type;
    // 记录ID
    private Long id;
    // 记录内容
    private Map<String, Object> doc;
    // 处理类型
    private OpType opType;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Item [timestamp=");
        builder.append(timestamp);
        builder.append(", source=");
        builder.append(source);
        builder.append(", lines=");
        builder.append(lines);
        builder.append(", type=");
        builder.append(type);
        builder.append(", id=");
        builder.append(id);
        builder.append(", opType=");
        builder.append(opType);
        builder.append(", doc=");
        builder.append(doc);
        builder.append(", results=");
        builder.append(results);
        builder.append("]");
        return builder.toString();
    }

    public List<LocalDateTime> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(List<LocalDateTime> timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, Object> getDoc() {
        return doc;
    }

    public void setDoc(Map<String, Object> doc) {
        this.doc = doc;
    }

    public ListMultimap<String, Map<String, Object>> getEffectDocs() {
        return effectDocs;
    }

    public ListMultimap<String, String> getResults() {
        return results;
    }

    public OpType getOpType() {
        return opType;
    }

    public void setOpType(OpType opType) {
        this.opType = opType;
    }

    public enum OpType {
        /** 更新或插入 */
        UPSERT,
        /** 更新(不存在时不插入) */
        UPDATE,
        /** 删除 */
        DELETE,
        /** 忽略 */
        DISCARD
    }

}
