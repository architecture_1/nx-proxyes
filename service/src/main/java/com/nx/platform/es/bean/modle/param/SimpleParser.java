package com.nx.platform.es.bean.modle.param;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年10月15日
 */
public class SimpleParser implements ParamFieldConfigParser {

    private static final Pattern FORMAT = Pattern.compile("[\\w.]+");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String face = MoreMaps.getString(settings, "face", "");
        String clazz = MoreMaps.getString(settings, "class");
        Preconditions.checkState(FORMAT.matcher(face).matches(), "param face empty or incorrect");
        String defaultValue = MoreMaps.getString(settings, "default", "");
        if (StringUtils.isNotEmpty(clazz)) {
            return ImmutableMap.of("face", face, "class", clazz, "default", defaultValue);
        } else {
            return ImmutableMap.of("face", face, "default", defaultValue);
        }
    }

}
