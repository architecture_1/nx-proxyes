package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;

/**
 * @author
 * @since 2016年11月16日
 */
public class ParamNumberTermsFilterParser implements ScoreFunctionFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String fieldName = MoreMaps.getString(settings, "fieldName");
        Preconditions.checkState(!Strings.isNullOrEmpty(fieldName), "param fieldName not found");
        String paramName = MoreMaps.getString(settings, "paramName");
        Preconditions.checkState(!Strings.isNullOrEmpty(paramName), "param paramName not found");
        float weight = MoreMaps.getFloatValue(settings, "weight", 0f);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("fieldName", fieldName);
        builder.put("paramName", paramName);
        builder.put("weight", weight);
        return builder.build();
    }

}
