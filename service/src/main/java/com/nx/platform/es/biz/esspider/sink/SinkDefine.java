package com.nx.platform.es.biz.esspider.sink;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author
 * @since 2017年3月22日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SinkDefine {

    /**
     * Sink类型
     *
     * @return
     */
    SinkType value();

}
