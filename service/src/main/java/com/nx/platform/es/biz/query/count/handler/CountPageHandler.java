package com.nx.platform.es.biz.query.count.handler;


import com.nx.platform.es.biz.query.count.CountRequestContext;

/**
 * @author
 * @since 2016年10月15日
 */
public enum CountPageHandler implements CountRequestHandler {

    INSTANCE;

    @Override
    public void handle(CountRequestContext context) {
        context.getRequest().source().size(0);
        context.getTrace().append("&size=0");
    }

}
