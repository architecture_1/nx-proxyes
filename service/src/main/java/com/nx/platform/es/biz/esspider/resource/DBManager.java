package com.nx.platform.es.biz.esspider.resource;


import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author
 * @date 2018/01/25
 */
public interface DBManager {

    NamedParameterJdbcTemplate acquireJdbcTemplate(String id);

    void close();

}
