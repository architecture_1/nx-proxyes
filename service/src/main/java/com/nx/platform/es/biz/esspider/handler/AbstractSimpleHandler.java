package com.nx.platform.es.biz.esspider.handler;

import java.util.Map;

/**
 * @author
 * @since 2017年3月27日
 */
public abstract class AbstractSimpleHandler implements Handler {

    public AbstractSimpleHandler() {}

    @Override
    public void init(Map<?, ?> settings) throws Exception {
        // Nothing
    }

}
