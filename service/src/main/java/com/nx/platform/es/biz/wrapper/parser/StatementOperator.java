package com.nx.platform.es.biz.wrapper.parser;

/**
 * @author
 * @since 2016年10月15日
 */
public enum StatementOperator {

    EQUAL("=", true), NOT_EQUAL("!=", false);

    private final String symbol;
    private final boolean sign;

    StatementOperator(String symbol, boolean sign) {
        this.symbol = symbol;
        this.sign = sign;
    }

    public static StatementOperator value(String symbol) {
        for (StatementOperator oprator : StatementOperator.values()) {
            if (oprator.symbol.equals(symbol)) {
                return oprator;
            }
        }
        throw new IllegalArgumentException("no such operator found: " + symbol);
    }

    public String getSymbol() {
        return symbol;
    }

    public boolean isSign() {
        return sign;
    }

}
