package com.nx.platform.es.common.utils;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.Set;

/**
 * @author
 * @since 2016年11月15日
 */
public class MoreSets {

    /**
     * 并集
     *
     * @param set1
     * @param set2
     * @return
     */
    public static <T> Set<T> union(final Set<T> set1, final Set<T> set2) {
        if (set1 == null && set2 == null) {
            return ImmutableSet.of();
        } else if (set1 == null) {
            return set2;
        } else if (set2 == null) {
            return set1;
        } else {
            return Sets.union(set1, set2);
        }
    }

    /**
     * 并集
     *
     * @param set1
     * @param set2
     * @param set3
     * @return
     */
    public static <T> Set<T> union(final Set<T> set1, final Set<T> set2, final Set<T> set3) {
        return union(union(set1, set2), set3);
    }

    /**
     * 交集
     *
     * @param set1
     * @param set2
     * @return
     */
    public static <T> Set<T> intersection(final Set<T> set1, final Set<?> set2) {
        if (set1 == null || set2 == null) {
            return ImmutableSet.of();
        } else {
            return Sets.intersection(set1, set2);
        }
    }

    /**
     * @param set
     * @param e
     * @return
     */
    public static <T> Set<T> add(Set<T> set, T e) {
        if (set == null) {
            set = Sets.newHashSet();
        }
        if (e != null) {
            set.add(e);
        }
        return set;
    }

    /**
     * @param set
     * @param e
     * @return
     */
    public static <T> boolean contains(Set<T> set, T e) {
        return set != null && e != null && set.contains(e);
    }

    /**
     * @param set
     * @return
     */
    public static boolean isEmpty(Set<?> set) {
        return set == null || set.isEmpty();
    }

    /**
     * @param set
     * @return
     */
    public static boolean isNotEmpty(Set<?> set) {
        return set != null && !set.isEmpty();
    }

    /**
     * @param set
     * @return
     */
    public static <T> Set<T> nullToEmpty(Set<T> set) {
        if (set == null) {
            return Collections.emptySet();
        }
        return set;
    }

}
