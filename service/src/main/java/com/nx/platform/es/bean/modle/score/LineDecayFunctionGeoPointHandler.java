package com.nx.platform.es.bean.modle.score;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.MapUtils;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;
import org.elasticsearch.index.query.functionscore.LinearDecayFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class LineDecayFunctionGeoPointHandler implements ScoreFunctionFieldHandler {

    @Override
    public FilterFunctionBuilder handle(ImmutableMap<String, ?> fieldConfig, Map<String, Object> params) {
        double lon = MapUtils.getDoubleValue(params, "lon", 0d);
        double lat = MapUtils.getDoubleValue(params, "lat", 0d);
        if (Double.doubleToLongBits(lon) == 0 && Double.doubleToLongBits(lat) == 0) {
            return null;
        }
        String fieldName = MapUtils.getString(fieldConfig, "fieldName");
        String scale = MapUtils.getString(fieldConfig, "scale");
        String offset = MapUtils.getString(fieldConfig, "offset");
        double decay = MapUtils.getDoubleValue(fieldConfig, "decay", 0.5d);
        float weight = MapUtils.getFloatValue(fieldConfig, "weight", 0f);
        LinearDecayFunctionBuilder function = ScoreFunctionBuilders
                .linearDecayFunction(fieldName, new GeoPoint(lat, lon), scale, offset, decay);
        if (weight > 0) {
            function.setWeight(weight);
        }
        return new FilterFunctionBuilder(function);
    }

}
