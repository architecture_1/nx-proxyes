package com.nx.platform.es.system.init;

import com.nx.platform.es.service.impl.MQNotifierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 依赖rpc 框架关闭，先注释
 * @author
 * @date 2018/04/12
 */
@Component
public class Shutdown  {
//public class Shutdown implements IShutdown {
//
//    @Autowired
//    private MQNotifierService app;
//
//    @Override
//    public void shutdown() {
//        app.stopAsync().awaitTerminated();
//    }

}
