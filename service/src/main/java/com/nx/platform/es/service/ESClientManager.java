package com.nx.platform.es.service;

import org.elasticsearch.client.RestHighLevelClient;

import java.util.Set;

/**
 * ES客户端资源管理
 * <p>
 * Created by  on 2017/4/14.
 */
public interface ESClientManager {

    /**
     * @param codes 指定code
     * @param target 当code为空，使用target去查找单一的client
     * @return ES客户端资源对象
     */
    ESClient getResource(Set<String> codes, String target);

    final class ESClient {

        private final String code;
        private final String hosts;
        private final RestHighLevelClient client;

        public ESClient(String code, String hosts, RestHighLevelClient client) {
            this.code = code;
            this.hosts = hosts;
            this.client = client;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("ESClient{");
            sb.append("code='").append(code).append('\'');
            sb.append(", hosts=").append(hosts);
            sb.append('}');
            return sb.toString();
        }

        public String getCode() {
            return code;
        }

        public RestHighLevelClient getClient() {
            return client;
        }

        public String getHosts() {
            return hosts;
        }

    }

}
