package com.nx.platform.es.biz.query.count.handler;


import com.nx.platform.es.biz.query.count.CountRequestContext;

/**
 * @author
 * @since 2016年10月15日
 */
public interface CountRequestHandler {

    /**
     * @param config
     * @param context
     * @return
     */
    void handle(CountRequestContext context);

}
