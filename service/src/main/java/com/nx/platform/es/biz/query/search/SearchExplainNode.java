package com.nx.platform.es.biz.query.search;

import com.google.common.primitives.Ints;
import com.nx.platform.es.common.utils.Constants;
import com.nx.platform.es.system.config.ConfigCenter;

import java.util.List;

/**
 * Created by secwang on 09/05/2017.
 */
public class SearchExplainNode {

    private Float value;
    private String description;
    private Integer level;
    private List<SearchExplainNode> details;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SearchExplainNode> getDetails() {
        return details;
    }

    public void setDetails(List<SearchExplainNode> details) {
        this.details = details;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    // DFS version
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.value == 0.0) {
            return sb.toString();
        } else {
            if (level < getConfigExplainLevel()) {
                String indent = new String(new char[level]).replace("\0", "  ");
                sb.append(indent).append(this.value).append(" ").append(this.description.replace('\n', ' ')).append("\n");
            }
        }
        for (SearchExplainNode node : this.getDetails()) {
            sb.append(node.toString());
        }
        return sb.toString();
    }

    private int getConfigExplainLevel() {
        return (Integer) ConfigCenter.getConfig(Constants.EXPLAIN_LEVEL, Ints::tryParse).orElse(Constants.EXPLAIN_LEVEL_DEFAULT);
    }

}
