package com.nx.platform.es.bean.modle.param;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年12月1日
 */
public class GeoPointParser implements ParamFieldConfigParser {

    private static final Pattern FORMAT = Pattern.compile("\\w++");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String faceLat = MoreMaps.getString(settings, "faceLat", "lat");
        String faceLon = MoreMaps.getString(settings, "faceLon", "lon");
        Preconditions.checkState(FORMAT.matcher(faceLat).matches(), "param faceLat empty or incorrect");
        Preconditions.checkState(FORMAT.matcher(faceLon).matches(), "param faceLon empty or incorrect");
        return ImmutableMap.of("faceLat", faceLat, "faceLon", faceLon);
    }

}
