package com.nx.platform.es.biz.query.search;

import com.google.common.collect.Table;
import com.nx.platform.es.bean.dto.RequestContext;
import com.nx.platform.es.bean.modle.param.ParamField;
import com.nx.platform.es.bean.modle.query.QueryField;
import com.nx.platform.es.bean.modle.rescore.RescoreField;
import com.nx.platform.es.bean.modle.score.ScoreFunctionField;
import com.nx.platform.es.bean.modle.sort.SortField;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import org.elasticsearch.search.sort.SortOrder;

import java.util.List;

/**
 * @author
 * @since 2016年10月15日
 */
public class SearchRequestContext extends RequestContext {

    private boolean fetchFields;
    private boolean fetchSource;
    private boolean explain;

    private List<QueryField> queryFields;
    private List<ParamField> paramFields;
    private List<SortField> sortFields;
    private List<ScoreFunctionField> scoreFunctionFields;
    // 打分函数与Query得分如何结合 multiply、replace、sum、avg、max、min
    private String boostMode;
    // 打分函数之间如何结合 multiply、sum、avg、first、max、min
    private String scoreMode;
    private List<RescoreField> rescoreFields;
    private SortOrder sortOrder;

    private Table<String, StatementOperator, List<String>> queryTable;
    private Table<String, StatementOperator, List<String>> paramsTable;

    public SearchRequestContext(String logPrefix, String entry, String query, String params) {
        super(logPrefix, entry, query, params);
    }

    public boolean isFetchFields() {
        return fetchFields;
    }

    public void setFetchFields(boolean fetchFields) {
        this.fetchFields = fetchFields;
    }

    public boolean isFetchSource() {
        return fetchSource;
    }

    public void setFetchSource(boolean fetchSource) {
        this.fetchSource = fetchSource;
    }

    public boolean isExplain() {
        return explain;
    }

    public void setExplain(boolean explain) {
        this.explain = explain;
    }

    public List<QueryField> getQueryFields() {
        return queryFields;
    }

    public void setQueryFields(List<QueryField> queryFields) {
        this.queryFields = queryFields;
    }

    public List<ParamField> getParamFields() {
        return paramFields;
    }

    public void setParamFields(List<ParamField> paramFields) {
        this.paramFields = paramFields;
    }

    public List<SortField> getSortFields() {
        return sortFields;
    }

    public void setSortFields(List<SortField> sortFields) {
        this.sortFields = sortFields;
    }

    public List<ScoreFunctionField> getScoreFunctionFields() {
        return scoreFunctionFields;
    }

    public void setScoreFunctionFields(List<ScoreFunctionField> scoreFunctionFields) {
        this.scoreFunctionFields = scoreFunctionFields;
    }

    public List<RescoreField> getRescoreFields() {
        return rescoreFields;
    }

    public void setRescoreFields(List<RescoreField> rescoreFields) {
        this.rescoreFields = rescoreFields;
    }

    public Table<String, StatementOperator, List<String>> getQueryTable() {
        return queryTable;
    }

    public void setQueryTable(Table<String, StatementOperator, List<String>> queryTable) {
        this.queryTable = queryTable;
    }

    public Table<String, StatementOperator, List<String>> getParamsTable() {
        return paramsTable;
    }

    public void setParamsTable(Table<String, StatementOperator, List<String>> paramsTable) {
        this.paramsTable = paramsTable;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getBoostMode() {
        return boostMode;
    }

    public void setBoostMode(String boostMode) {
        this.boostMode = boostMode;
    }

    public String getScoreMode() {
        return scoreMode;
    }

    public void setScoreMode(String scoreMode) {
        this.scoreMode = scoreMode;
    }

}
