package com.nx.platform.es.biz.wrapper.parser;

import com.google.common.collect.Table;

import java.util.List;

/**
 * @author
 * @since 2016年10月15日
 */
public interface StatementParser {

    /**
     * @param statement
     * @return
     */
    Table<String, StatementOperator, List<String>> parse(String statement);

}
