package com.nx.platform.es.biz.query.count.handler;

import com.google.common.collect.Table;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import com.nx.platform.es.biz.query.count.CountRequestContext;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;


/**
 * @author
 * @since 2016年12月1日
 */
public enum CountCacheHandler implements CountRequestHandler {

    INSTANCE;

    @Override
    public void handle(CountRequestContext context) {
        Table<String, StatementOperator, List<String>> queryTable = context.getQueryTable();
        List<String> list = queryTable.get("nocache", StatementOperator.EQUAL);
        context.setNocache(CollectionUtils.isNotEmpty(list) && "1".equals(list.get(0)));
    }

}
