package com.nx.platform.es.common.utils;

import java.util.List;

/**
 * @author
 * @since Jan 16, 2017
 */
public class MoreLists {

    public static <T> T getFirst(final List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
