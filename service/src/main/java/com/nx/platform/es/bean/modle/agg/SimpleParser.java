package com.nx.platform.es.bean.modle.agg;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年10月15日
 */
public class SimpleParser implements AggFieldConfigParser {

    private static final Pattern FORMAT = Pattern.compile("[\\w.]+");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String face = MoreMaps.getString(settings, "face", "");
        String fieldName = MoreMaps.getString(settings, "fieldName", "");
        if ("".equals(fieldName)) {
            fieldName = face;
        }
        Preconditions.checkState(FORMAT.matcher(face).matches(), "param face empty or incorrect: " + face);
        Preconditions.checkState(FORMAT.matcher(fieldName).matches(), "param fieldName empty or incorrect" + fieldName);
        Integer size = MoreMaps.getInteger(settings, "_size", 20);
        return ImmutableMap.of("face", face, "fieldName", fieldName, "_size", size);
    }

}
