package com.nx.platform.es.biz.query.search.handler;


import com.nx.platform.es.biz.query.search.SearchRequestContext;

/**
 * @author
 * @since 2016年10月15日
 */
public interface SearchRequestHandler {

    /**
     * @param context 请求上下文
     */
    void handle(SearchRequestContext context);

}
