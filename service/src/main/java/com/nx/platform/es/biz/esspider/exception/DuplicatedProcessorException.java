package com.nx.platform.es.biz.esspider.exception;


import com.nx.platform.es.biz.esspider.process.Processor;

/**
 * @author
 * @since 2017年3月25日
 */
public class DuplicatedProcessorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DuplicatedProcessorException(Processor processor) {
        super(processor != null ? processor.getCode() : null);
    }

}
