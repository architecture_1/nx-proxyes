package com.nx.platform.es.biz.esspider.handler;

import com.nx.platform.es.biz.esspider.entity.Item;

import java.util.Map;

/**
 * @author
 * @date 2018/01/26
 */
public interface Handlers {

    /**
     * 检查是否正常
     *
     * @param code
     * @throws RuntimeException
     */
    void check(String code) throws RuntimeException;

    /**
     * 执行过滤处理
     *
     * @param code
     * @param items
     * @return
     * @throws Exception
     */
    void filter(String code, Map<Long, Item> items) throws Exception;

}
