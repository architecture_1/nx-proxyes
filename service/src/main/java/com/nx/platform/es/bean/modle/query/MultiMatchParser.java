package com.nx.platform.es.bean.modle.query;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年10月15日
 */
public class MultiMatchParser implements QueryFieldConfigParser {

    private static final Pattern FORMAT_FACE = Pattern.compile("[\\w.]++");
    private static final Pattern FORMAT_FIELDNAME = Pattern
            .compile("[\\w.]++(?:\\^\\d++(?:\\.\\d++)?+)?+(?:,[\\w.]++(?:\\^\\d++(?:\\.\\d++)?+)?+)*+");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        //
        String face = MoreMaps.getString(settings, "face");
        String fieldName = MoreMaps.getString(settings, "fieldName", "");
        Preconditions.checkState(FORMAT_FACE.matcher(face).matches(), "param face empty or incorrect: " + face);
        Preconditions.checkState(FORMAT_FIELDNAME.matcher(fieldName).matches(),
                "param fieldName empty or incorrect: " + fieldName);
        //
        List<String> _fields = Splitter.on(",").omitEmptyStrings().splitToList(fieldName);
        String _type = MoreMaps.getString(settings, "_type", "");
        String _operator = MoreMaps.getString(settings, "_operator", "");
        String _minimum_should_match = MoreMaps.getString(settings, "_minimum_should_match", "");
        String _analyzer = MoreMaps.getString(settings, "_analyzer", "");
        Float _tie_breaker = MoreMaps.getFloat(settings, "_tie_breaker", 0f);
        Integer alternatives = MoreMaps.getInteger(settings, "_alternatives", 8);
        Preconditions.checkState(alternatives <= 16 && alternatives > 1, "param alternatives incorrect");
        //
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("face", face);
        builder.put("fieldName", fieldName);
        builder.put("_fields", _fields);
        builder.put("_type", _type);
        builder.put("_operator", _operator);
        builder.put("_minimum_should_match", _minimum_should_match);
        builder.put("_analyzer", _analyzer);
        builder.put("_tie_breaker", _tie_breaker);
        builder.put("_alternatives", alternatives);
        return builder.build();
    }

}
