package com.nx.platform.es.biz.query.agg;


import com.google.common.collect.Table;
import com.nx.platform.es.bean.dto.RequestContext;
import com.nx.platform.es.bean.modle.param.ParamField;
import com.nx.platform.es.bean.modle.query.QueryField;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import com.nx.platform.es.bean.modle.agg.AggField;

import java.util.List;

/**
 * @author
 * @since 2016年10月21日
 */
public class AggRequestContext extends RequestContext {

    private List<QueryField> queryFields;
    private List<ParamField> paramFields;
    private List<AggField> aggFields;

    private Table<String, StatementOperator, List<String>> queryTable;
    private Table<String, StatementOperator, List<String>> paramsTable;

    public AggRequestContext(String logPrefix, String entry, String query, String params) {
        super(logPrefix, entry, query, params);
    }

    public List<AggField> getAggFields() {
        return aggFields;
    }

    public void setAggFields(List<AggField> aggFields) {
        this.aggFields = aggFields;
    }

    public List<QueryField> getQueryFields() {
        return queryFields;
    }

    public void setQueryFields(List<QueryField> queryFields) {
        this.queryFields = queryFields;
    }

    public List<ParamField> getParamFields() {
        return paramFields;
    }

    public void setParamFields(List<ParamField> paramFields) {
        this.paramFields = paramFields;
    }

    public Table<String, StatementOperator, List<String>> getQueryTable() {
        return queryTable;
    }

    public void setQueryTable(Table<String, StatementOperator, List<String>> queryTable) {
        this.queryTable = queryTable;
    }

    public Table<String, StatementOperator, List<String>> getParamsTable() {
        return paramsTable;
    }

    public void setParamsTable(Table<String, StatementOperator, List<String>> paramsTable) {
        this.paramsTable = paramsTable;
    }

}
