package com.nx.platform.es.bean.modle.sort;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;
import org.elasticsearch.search.sort.SortOrder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class PainlessScriptSortParser implements SortFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String inline = MoreMaps.getString(settings, "inline");
        Preconditions.checkState(!Strings.isNullOrEmpty(inline), "inline not found");
        String order = MoreMaps.getString(settings, "order");
        if (!org.elasticsearch.common.Strings.isNullOrEmpty(order)) {
            if ("desc".equalsIgnoreCase(order)) {
                return ImmutableMap.of("inline", inline, "order", SortOrder.DESC);
            } else if ("asc".equalsIgnoreCase(order)) {
                return ImmutableMap.of("inline", inline, "order", SortOrder.ASC);
            }
        }
        return ImmutableMap.of("inline", inline);
    }

}
