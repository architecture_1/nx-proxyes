package com.nx.platform.es.system.db;


import com.nx.arch.rdb.sharding.api.ShardingValue;
import com.nx.arch.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;

import java.util.Collection;

public class TradeDatabaseShardingAlgorithm implements SingleKeyDatabaseShardingAlgorithm<Long> {

    @Override
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Long> shardingValue) {
        return chooseDb(availableTargetNames, "0");
    }

    @Override
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Long> shardingValue) {
        return availableTargetNames;
    }

    @Override
    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Long> shardingValue) {
       return availableTargetNames;
    }

    private String chooseDb(Collection<String> availableTargetNames, String tag) {
        return availableTargetNames.iterator().next();
    }

}
