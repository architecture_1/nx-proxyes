package com.nx.platform.es.bean.modle.sort;

import com.google.common.collect.ImmutableMap;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface SortFieldHandler {

    /**
     * @param fieldConfig
     * @param params
     * @return
     */
    SortBuilder<?> handle(ImmutableMap<String, ?> fieldConfig, Map<String, Object> params, SortOrder sortOrder);

}
