package com.nx.platform.es.bean.modle.query;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ListMultimap;
import com.google.common.primitives.Floats;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import com.nx.platform.es.bean.dto.RequestContext;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author
 * @since 2016年10月13日
 */
public class StringMatchShouldHandler implements QueryFieldHandler {

    @Override
    public void handle(ImmutableMap<String, ?> fieldConfig, RequestContext context, String fieldFace,
                       StatementOperator operator, String fieldValue, ListMultimap<Boolean, QueryBuilder> queryBuilders) {
        //
        if (Strings.isNullOrEmpty(fieldValue)) {
            return;
        }
        // 记录Query信息(Trace)
        context.getTrace().append("&").append(fieldFace).append(operator.getSymbol()).append(fieldValue);
        //
        String face = MapUtils.getString(fieldConfig, "face");
        String fieldName = MapUtils.getString(fieldConfig, "fieldName");
        int alternatives = MapUtils.getIntValue(fieldConfig, "_alternatives", 8);
        Preconditions.checkState(!Strings.isNullOrEmpty(fieldName));
        //
        List<String> words = Splitter.on("|").omitEmptyStrings().splitToList(fieldValue);
        if (words.isEmpty()) {
            return;
        }
        if (words.size() > alternatives) {
            throw new IllegalArgumentException("too more alternatives: field=" + face + ", value=" + fieldValue);
        }
        queryBuilders.putAll(operator.isSign(), words.stream()
                .map(word -> build(fieldName, word))
                .collect(Collectors.toList()));
    }

    private MatchQueryBuilder build(String fieldName, String fieldValue) {
        Pair<String, Float> fieldBoost = stringAndBoost(fieldName, 1.0f);
        Pair<String, Float> valueBoost = stringAndBoost(fieldValue, 1.0f);
        double boost = fieldBoost.getValue().doubleValue() * valueBoost.getValue().doubleValue();
        return QueryBuilders.matchQuery(fieldBoost.getKey(), valueBoost.getKey())
                .boost((float)boost)
                .operator(Operator.AND);
    }

    private Pair<String, Float> stringAndBoost(String origin, Float defaultBoost) {
        int index = origin.indexOf('^');
        if (index > 0) {
            String str = origin.substring(0, index);
            Float boost = Floats.tryParse(origin.substring(index + 1));
            return Pair.of(str, boost != null && boost > 0 ? boost : defaultBoost);
        } else {
            return Pair.of(origin, defaultBoost);
        }
    }

}
