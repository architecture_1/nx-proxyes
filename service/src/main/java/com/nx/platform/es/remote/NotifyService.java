package com.nx.platform.es.remote;


import com.nx.platform.es.contract.INotifyService;
import com.nx.platform.es.service.impl.MQNotifierService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author alex
 * @brief
 * @date 2019/10/17
 */
public class NotifyService implements INotifyService {

    @Autowired
    private MQNotifierService app;

    @Override
    public void notifyTest(String logStr, String message) {
        app.scfSourceService().notify(logStr, message);
    }
}
