package com.nx.platform.es.biz.query.count.handler;

import com.nx.platform.es.biz.query.count.CountRequestContext;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchType;

/**
 * @author
 * @since 2016年10月15日
 */
public enum CountIndexHandler implements CountRequestHandler {

    INSTANCE;

    @Override
    public void handle(CountRequestContext context) {
        SearchRequest request = context.getRequest();
        request.indices(context.getIndex()).types(context.getType());
        request.searchType(SearchType.QUERY_THEN_FETCH);
        context.getTrace().append("&_entry=").append(context.getEntry());
    }

}
