package com.nx.platform.es.common.utils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import java.math.BigInteger;

/**
 * @author
 * @date 2018/01/30
 */
public class Utils {

    @SuppressWarnings("deprecation")
    public static String toMD5(Object src) {
        return Hashing.md5().hashString(String.valueOf(src), Charsets.UTF_8).toString();
    }

    public static long merge(int high, int low) {
        return ((long) low & 0xFFFFFFFFL) | (((long) high << 32) & 0xFFFFFFFF00000000L);
    }

    public static String merge(int high, long low) {
        return BigInteger.valueOf((long) high).shiftLeft(64).add(BigInteger.valueOf(low)).toString();
    }

}
