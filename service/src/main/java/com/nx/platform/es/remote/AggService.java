package com.nx.platform.es.remote;


import com.nx.platform.es.biz.query.agg.AggRequestContext;
import com.nx.platform.es.biz.query.agg.AggRequestProcessor;
import com.nx.platform.es.contract.IAggService;
import com.nx.platform.es.entity.response.TermsAggResult;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author alex
 * @brief
 * @date 2019/11/4
 */
public class AggService implements IAggService {

    @Autowired
    private AggRequestProcessor aggRequestProcessor;

    @Override
    public TermsAggResult termsAgg(String logStr, String index, String query, String params) throws Exception {
        AggRequestContext context = new AggRequestContext(logStr, index, query, params);
        return aggRequestProcessor.request(context);
    }
}
