package com.nx.platform.es.bean.modle.agg;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface AggFieldConfigParser {

    /**
     * @param map
     * @return
     */
    ImmutableMap<String, ?> parse(Map<?, ?> settings);

}
