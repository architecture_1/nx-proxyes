package com.nx.platform.es.biz.esspider.process;

import com.nx.platform.es.biz.esspider.entity.Message;

/**
 * @author
 * @date 2018/04/13
 */
public interface Processor {
    /**
     * 处理器code
     *
     * @return
     */
    String getCode();

    /**
     * 传递一个消息
     *
     * @param message
     * @throws Exception
     */
    void deliver(Message message) throws Exception;

}
