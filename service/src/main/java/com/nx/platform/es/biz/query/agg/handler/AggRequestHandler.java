package com.nx.platform.es.biz.query.agg.handler;


import com.nx.platform.es.biz.query.agg.AggRequestContext;

/**
 * @author
 * @since 2016年10月15日
 */
public interface AggRequestHandler {

    /**
     * @param config
     * @param context
     * @return
     */
    void handle(AggRequestContext context);

}
