package com.nx.platform.es.biz.query.count.cache;

import com.nx.platform.es.common.utils.Constants;
import com.nx.platform.es.system.config.ConfigCenter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @date 2018/02/28
 */
public class CountCaches {

    private static final String DEFAULT = CodisCountCache.class.getSimpleName();
    private static final CountCache NULL = new NulllCountCache();
    private final Map<String, CountCache> map;

    public CountCaches(CountCache... caches) {
        map = new HashMap<>();
        for (CountCache cache : caches) {
            map.put(cache.getIdentity(), cache);
        }
    }

    @NotNull
    public CountCache getCache() {
        return (CountCache) ConfigCenter
                .getConfig(Constants.CACHE_COUNT, map::get)
                .orElse(map.getOrDefault(DEFAULT, NULL));
    }

}
