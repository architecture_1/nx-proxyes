package com.nx.platform.es.bean.modle.sort;

import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.sort.SortOrder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class SimpleParser implements SortFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String order = MoreMaps.getString(settings, "order");
        if (!Strings.isNullOrEmpty(order)) {
            if ("desc".equalsIgnoreCase(order)) {
                return ImmutableMap.of("order", SortOrder.DESC);
            } else if ("asc".equalsIgnoreCase(order)) {
                return ImmutableMap.of("order", SortOrder.ASC);
            }
        }
        return ImmutableMap.of();
    }

}
