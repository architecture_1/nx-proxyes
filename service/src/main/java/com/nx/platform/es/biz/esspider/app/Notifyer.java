package com.nx.platform.es.biz.esspider.app;

import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.nx.platform.es.biz.esspider.reactor.NotifyReactor;
import com.nx.platform.es.biz.esspider.reactor.Reactor;
import com.nx.platform.es.biz.esspider.source.InvokeSourceService;
import lombok.extern.slf4j.Slf4j;

/**
 * 用于对MQ 消息进行索引更新
 * @author
 * @date 2018/04/13
 */
@Slf4j
public class Notifyer extends AbstractExecutionThreadService {

    private final Reactor reactor;

    private InvokeSourceService scfSourceService;

    public Notifyer(String configFilePath, String dataFilePath, String configNamespace) {
        log.info("notifyer init {} {} {}", configFilePath, dataFilePath, configNamespace);
        this.reactor = NotifyReactor.builder()
                .configFilePath(configFilePath)
                .dataFilePath(dataFilePath)
                .configNamespace(configNamespace)
                .build();

    }

    public InvokeSourceService scfSourceService() {
        if (scfSourceService == null) {
            scfSourceService = reactor.getSource(InvokeSourceService.class);
        }
        return scfSourceService;
    }

    @Override
    protected void startUp() throws Exception {
        log.info("notifier starup");
        reactor.startAsync().awaitRunning();
    }

    @Override
    public void run() {
        reactor.awaitTerminated();
    }

    @Override
    protected void triggerShutdown() {
        if (reactor != null && reactor.state() != State.FAILED) {
            reactor.stopAsync().awaitTerminated();
        }
    }

}
