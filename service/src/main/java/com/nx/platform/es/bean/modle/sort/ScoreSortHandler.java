package com.nx.platform.es.bean.modle.sort;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.MapUtils;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class ScoreSortHandler implements SortFieldHandler {

    @Override
    public SortBuilder<?> handle(ImmutableMap<String, ?> fieldConfig, Map<String, Object> params, SortOrder sortOrder) {
        Object order = MapUtils.getObject(fieldConfig, "order");
        return SortBuilders.scoreSort().order((order instanceof SortOrder) ? (SortOrder) order : SortOrder.DESC);
    }

}
