package com.nx.platform.es.system.init;

import com.nx.platform.es.service.impl.MQNotifierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

import javax.annotation.PostConstruct;
import java.io.File;

@Slf4j
@Configuration // 声明 这是一个JavaConfig配置文件
@ImportResource(value = {"classpath*:spring.xml"}) // 引入classpath下面的 spring-context.xml 配置文件
@Order(1)
public class SCFConfiguration {


    @Autowired
    MQNotifierService app;

    @PostConstruct
    public void init() {

        /*
         * 初始化SCF配置
         */
        log.info("========begin init ============");
        if (new File(SystemInit.SCF_KEY_PATH).exists()) {
//            SCFInit.initScfKey(SystemInit.SCF_KEY_PATH);
        }
//        SCFInit.init(SystemInit.SCF_CONFIG_PATH);
        System.out.println("end scf init");

//        new MQConsumer().start();
        //初始化线程模型
        app.startAsync().awaitRunning();
        /*// 服务内有用到idc生成ID就需要IdcMachineCode，没有就不需要
        if (!IdcMachineCode.init()) {
            System.exit(0);
        }*/
    }
}