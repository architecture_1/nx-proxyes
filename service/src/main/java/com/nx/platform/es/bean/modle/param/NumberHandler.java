package com.nx.platform.es.bean.modle.param;

import com.google.common.base.Enums;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Table;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.nx.platform.es.biz.wrapper.parser.StatementOperator;
import com.nx.platform.es.bean.dto.RequestContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.Strings;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class NumberHandler implements ParamFieldHandler {

    @Override
    public void handle(RequestContext context, ImmutableMap<String, ?> fieldConfig,
                       Table<String, StatementOperator, List<String>> queryTable,
                       Table<String, StatementOperator, List<String>> paramsTable, Map<String, Object> paramsMap) {
        String defaultValue = MapUtils.getString(fieldConfig, "default");
        String fieldFace = MapUtils.getString(fieldConfig, "face");
        if (Strings.isNullOrEmpty(fieldFace)) {
            return;
        }
        List<String> fieldValue = paramsTable.get(fieldFace, StatementOperator.EQUAL);
        String clazz = MapUtils.getString(fieldConfig, "class");
        if (CollectionUtils.isNotEmpty(fieldValue)) {
            Object param = parse(clazz, fieldValue.get(0));
            if (param != null) {
                context.getTrace().append("&_param_").append(fieldFace)
                        .append(StatementOperator.EQUAL.getSymbol()).append(fieldValue.get(0));
                paramsMap.put(fieldFace, param);
                return;
            }
        }
        if (StringUtils.isNotBlank(defaultValue)) {
            Object param = parse(clazz, defaultValue);
            if (param != null) {
                context.getTrace().append("&_param_").append(fieldFace)
                        .append(StatementOperator.EQUAL.getSymbol()).append(defaultValue);
                paramsMap.put(fieldFace, param);
            }
        }
    }

    protected Comparable<? extends Number> parse(String clazz, String str) {
        return Enums.getIfPresent(Parsers.class, clazz).or(Parsers.Long).parse(str);
    }

    enum Parsers {
        Byte {
            @Override
            Comparable<? extends Number> parse(String str) {
                try {
                    return java.lang.Byte.parseByte(str);
                } catch (NumberFormatException nfe) {
                    return null;
                }
            }
        },
        Short {
            @Override
            Comparable<? extends Number> parse(String str) {
                try {
                    return java.lang.Short.parseShort(str);
                } catch (NumberFormatException nfe) {
                    return null;
                }
            }
        },
        Integer {
            @Override
            Comparable<? extends Number> parse(String str) {
                return Ints.tryParse(str);
            }
        },
        Long {
            @Override
            Comparable<? extends Number> parse(String str) {
                return Longs.tryParse(str);
            }
        };

        abstract Comparable<? extends Number> parse(String str);

    }

}
