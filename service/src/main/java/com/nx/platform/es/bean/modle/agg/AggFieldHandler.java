package com.nx.platform.es.bean.modle.agg;

import com.google.common.collect.ImmutableMap;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface AggFieldHandler {

    /**
     * @param fieldConfig
     * @param params
     * @return
     */
    TermsAggregationBuilder handle(ImmutableMap<String, ?> fieldConfig, Map<String, ?> params);

}
