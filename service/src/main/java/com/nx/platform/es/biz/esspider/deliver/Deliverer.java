package com.nx.platform.es.biz.esspider.deliver;

import com.nx.platform.es.biz.esspider.process.Processor;
import com.nx.platform.es.biz.esspider.entity.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 消息分发路由
 *
 * @author
 * @since 2017年3月24日
 */
public interface Deliverer {

    Logger LOGGER = LogManager.getLogger(Deliverer.class);

    /**
     * 注册处理器
     *
     * @param processor 处理器
     * @throws RuntimeException
     */
    void register(Processor processor) throws RuntimeException;

    /**
     * 投递消息
     *
     * @param message 消息
     * @throws RuntimeException
     */
    void deliver(Message message) throws RuntimeException;

}
