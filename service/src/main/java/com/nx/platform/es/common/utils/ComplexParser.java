package com.nx.platform.es.common.utils;

import com.google.gson.JsonSyntaxException;
import com.nx.platform.es.system.config.ApolloUtil;

import java.util.*;

/**
 * @program: nx-esproxy-service
 * @description: ${description}
 * @author: Mr.JY
 * @create: 2019-09-04 12:09
 **/
@SuppressWarnings("unchecked")
public enum ComplexParser {
    PARAM() {
        @Override
        public Map<String, List<Long>> parse(String value) throws JsonSyntaxException {
            List<ApolloUtil.GsonUtil.Param> params = ApolloUtil.GsonUtil.toParams(value);
            if (params != null) {
                Map<String, List<Long>> map = new HashMap<>(params.size());
                for (ApolloUtil.GsonUtil.Param param : params) {
                    if (param.getParamId() != null && param.getValueId() != null) {
                        List<Long> paramValues = MoreFunctions.toLongList(param.getValueId(), MoreSplitters.VERTICAL_OR_COMMA);
                        if (!paramValues.isEmpty()) {
                            map.put(String.valueOf(param.getParamId()), paramValues);
                        }
                    }
                }
                return map;
            }
            return Collections.emptyMap();
        }
    },
    PARAM2() {
        @Override
        public Map<String, List<Long>> parse(String value) throws JsonSyntaxException {
            List<ApolloUtil.GsonUtil.Param2> params = ApolloUtil.GsonUtil.toParams2(value);
            if (params != null) {
                Map<String, List<Long>> map = new HashMap<>(params.size());
                for (ApolloUtil.GsonUtil.Param2 param : params) {
                    if (param.getpId() != null && param.getvId() != null) {
                        List<Long> paramValues = MoreFunctions.toLongList(param.getvId(), MoreSplitters.VERTICAL_OR_COMMA);
                        if (!paramValues.isEmpty()) {
                            map.put(String.valueOf(param.getpId()), paramValues);
                        }
                    }
                }
                return map;
            }
            return Collections.emptyMap();
        }
    },
    SERVICES() {
        @Override
        public Set<Long> parse(String value) throws JsonSyntaxException {
            List<ApolloUtil.GsonUtil.Service> services = ApolloUtil.GsonUtil.toServices(value);
            if (services != null) {
                Set<Long> set = new HashSet<>(services.size());
                for (ApolloUtil.GsonUtil.Service service : services) {
                    if (service.getServiceId() != null) {
                        set.add(service.getServiceId());
                    }
                }
                return set;
            }
            return Collections.emptySet();
        }
    };

    public abstract <T> T parse(String value) throws JsonSyntaxException;
}
