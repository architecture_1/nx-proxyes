package com.nx.platform.es.biz.esspider.sink;


import com.nx.platform.es.biz.esspider.entity.Item;

import java.util.Map;

/**
 * @author
 * @date 2018/01/25
 */
public interface Sinks {

    /**
     * 检查是否正常
     *
     * @param code
     * @throws RuntimeException
     */
    void check(String code) throws RuntimeException;

    /**
     * @param code
     * @param items
     * @return
     */
    Map<Long, Item> accept(String code, Map<Long, Item> items);

}
