package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class PainlessScriptFunctionParser implements ScoreFunctionFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String inline = MoreMaps.getString(settings, "inline");
        Preconditions.checkState(!Strings.isNullOrEmpty(inline), "inline not found");
        Float weight = MoreMaps.getFloat(settings, "weight", 0f);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("inline", inline);
        builder.put("weight", weight);
        return builder.build();
    }

}
