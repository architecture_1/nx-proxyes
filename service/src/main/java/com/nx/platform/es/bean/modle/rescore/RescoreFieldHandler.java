package com.nx.platform.es.bean.modle.rescore;

import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.bean.modle.score.ScoreFunctionField;
import org.elasticsearch.search.rescore.RescorerBuilder;

import java.util.List;
import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface RescoreFieldHandler {

    /**
     * @param fieldConfig
     * @param params
     * @return
     */
    RescorerBuilder<?> handle(ImmutableMap<String, ?> fieldConfig, List<ScoreFunctionField> functions,
                              Map<String, Object> params);

}
