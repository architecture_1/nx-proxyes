package com.nx.platform.es.bean.modle.param;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreSplitters;
import com.nx.platform.es.common.utils.MoreMaps;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author
 * @since 2016年10月15日
 */
public class SltrKeywordsParser implements ParamFieldConfigParser {

    private static final Pattern FORMAT = Pattern.compile("[\\w.]+");

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        String face = MoreMaps.getString(settings, "face", "");
        Preconditions.checkState(FORMAT.matcher(face).matches(), "param face empty or incorrect");
        List<String> refers = MoreMaps.getStringList(settings, "refers", MoreSplitters.COMMA);
        if (CollectionUtils.isNotEmpty(refers)) {
            return ImmutableMap.of("face", face, "refers", refers);
        } else {
            return ImmutableMap.of("face", face);
        }
    }

}
