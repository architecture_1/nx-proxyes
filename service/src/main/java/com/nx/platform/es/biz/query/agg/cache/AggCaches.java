package com.nx.platform.es.biz.query.agg.cache;

import com.nx.platform.es.common.utils.Constants;
import com.nx.platform.es.system.config.ConfigCenter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @date 2018/02/28
 */
public class AggCaches {

    private static final String DEFAULT = CodisAggCache.class.getSimpleName();
    private static final AggCache NULL = new NullAggCache();
    private final Map<String, AggCache> map;

    public AggCaches(AggCache... caches) {
        map = new HashMap<>();
        for (AggCache cache : caches) {
            map.put(cache.getIdentity(), cache);
        }
    }

    @NotNull
    public AggCache getCache() {
        return (AggCache) ConfigCenter
                .getConfig(Constants.CACHE_AGG, map::get)
                .orElse(map.getOrDefault(DEFAULT, NULL));
    }

}
