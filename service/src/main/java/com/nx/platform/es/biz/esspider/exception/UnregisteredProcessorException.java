package com.nx.platform.es.biz.esspider.exception;

/**
 * @author
 * @since 2017年3月25日
 */
public class UnregisteredProcessorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnregisteredProcessorException(String code) {
        super(code);
    }

}
