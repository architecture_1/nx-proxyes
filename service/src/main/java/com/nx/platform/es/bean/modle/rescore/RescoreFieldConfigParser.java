package com.nx.platform.es.bean.modle.rescore;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public interface RescoreFieldConfigParser {

    /**
     * @param map
     * @return
     */
    ImmutableMap<String, ?> parse(Map<?, ?> settings);

}
