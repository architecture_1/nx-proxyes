package com.nx.platform.es.mq.consumer;

/**
 * handler接口
 */
public interface ConsumerHandler {
    /**
     * 消费MQ
     *
     * @param tag
     * @param key
     * @param body
     * @return
     */
    boolean consume(String tag, String key, String body);

}
