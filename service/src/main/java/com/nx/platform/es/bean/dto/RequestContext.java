package com.nx.platform.es.bean.dto;


import com.nx.platform.es.service.ESClientManager;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.TimeValue;

/**
 * @author
 * @since 2016年10月21日
 */
public class RequestContext {

    protected final StringBuilder trace = new StringBuilder();

    protected final String logPrefix;
    protected final String entry;
    protected final String query;
    protected final String params;
    protected ESQueryConfDTO config;

    protected String client;
    protected String[] index;
    protected String[] type;
    protected String[] fetch;
    protected TimeValue timeout;
    // 5 ±2.4km; 6 ±0.61km;
    protected int geohashLevel;
    // 不缓存
    protected boolean nocache = false;
    // 过期时间(秒)
    protected int expireAfterWrite;
    protected boolean useRequestCache = false;

    protected String cacheKey;
    protected ESClientManager.ESClient esClient;
    protected SearchRequest request;
    protected SearchResponse response;

    public RequestContext(String logPrefix, String entry, String query, String params) {
        this.logPrefix = logPrefix;
        this.entry = entry;
        this.query = query;
        this.params = params;
    }

    public String getLogPrefix() {
        return logPrefix;
    }

    public String getEntry() {
        return entry;
    }

    public String getQuery() {
        return query;
    }

    public String getParams() {
        return params;
    }

    public String[] getIndex() {
        return index;
    }

    public void setIndex(String[] index) {
        this.index = index;
    }

    public String[] getType() {
        return type;
    }

    public void setType(String[] type) {
        this.type = type;
    }

    public String[] getFetch() {
        return fetch;
    }

    public void setFetch(String[] fetch) {
        this.fetch = fetch;
    }

    public TimeValue getTimeout() {
        return timeout;
    }

    public void setTimeout(TimeValue timeout) {
        this.timeout = timeout;
    }

    public int getGeohashLevel() {
        return geohashLevel;
    }

    public void setGeohashLevel(int geohashLevel) {
        this.geohashLevel = geohashLevel;
    }

    public StringBuilder getTrace() {
        return trace;
    }

    public boolean isNocache() {
        return nocache;
    }

    public void setNocache(boolean nocache) {
        this.nocache = nocache;
    }

    public boolean isUseRequestCache() {
        return useRequestCache;
    }

    public void setUseRequestCache(boolean useRequestCache) {
        this.useRequestCache = useRequestCache;
    }

    public int getExpireAfterWrite() {
        return expireAfterWrite;
    }

    public void setExpireAfterWrite(int expireAfterWrite) {
        this.expireAfterWrite = expireAfterWrite;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public ESQueryConfDTO getConfig() {
        return config;
    }

    public void setConfig(ESQueryConfDTO config) {
        this.config = config;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public ESClientManager.ESClient getEsClient() {
        return esClient;
    }

    public void setEsClient(ESClientManager.ESClient esClient) {
        this.esClient = esClient;
    }

    public SearchRequest getRequest() {
        return request;
    }

    public void setRequest(SearchRequest request) {
        this.request = request;
    }

    public SearchResponse getResponse() {
        return response;
    }

    public void setResponse(SearchResponse response) {
        this.response = response;
    }

}
