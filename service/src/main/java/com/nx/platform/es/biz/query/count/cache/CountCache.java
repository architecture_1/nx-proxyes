package com.nx.platform.es.biz.query.count.cache;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 计数缓存接口
 * <p>
 * Created by  on 2017/4/17.
 */
public interface CountCache {

    Gson GSON = new GsonBuilder().enableComplexMapKeySerialization()
            .serializeSpecialFloatingPointValues().create();

    String CACHE_KAY_PREFIX = "es:count:";

    default String getIdentity() {
        return this.getClass().getSimpleName();
    }

    /**
     * 获取缓存中的搜索结果
     *
     * @param logStr 日志前缀
     * @param cacheKey 缓存Key
     * @return 缓存结果
     */
    Long get(final String logStr, final String cacheKey);

    /**
     * 查询结果放入缓存
     *
     * @param logStr 日志前缀
     * @param cacheKey 缓存Key
     * @param result 查询结果
     * @param seconds 失效时间
     */
    void put(final String logStr, final String cacheKey, final Long result, final int seconds);

}
