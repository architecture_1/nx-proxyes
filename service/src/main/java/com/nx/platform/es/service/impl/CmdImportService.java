package com.nx.platform.es.service.impl;

import com.google.common.base.Preconditions;
import com.nx.platform.es.biz.esspider.app.Importer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author alex
 * @brief
 * @date 2019/11/4
 */

@Component
@Slf4j
public class CmdImportService {
    private static final String CONFIG_FILE_PATH;
    private static final String DATA_FILE_PATH;
    private static final String CONFIG_NAMESPACE;
    private static final String TARGET_SINKS;
    private static final String SOURCE_FILES;

    static {
        CONFIG_FILE_PATH = System.getProperty("config_path", "config");
        DATA_FILE_PATH = System.getProperty("data_path", "data");
        CONFIG_NAMESPACE = System.getProperty("key_namespace");
        TARGET_SINKS = System.getProperty("target_sinks");
        SOURCE_FILES = System.getProperty("source_files");
    }

    public static void main(String[] args) throws Exception {
        Preconditions.checkArgument(StringUtils.isNotBlank(CONFIG_NAMESPACE), "key_namespace empty");
        Preconditions.checkArgument(StringUtils.isNotBlank(TARGET_SINKS), "target_sinks empty");
        Preconditions.checkArgument(StringUtils.isNotBlank(SOURCE_FILES), "source_files empty");
        log.info("begin  xxxxxx");
        Importer.run(CONFIG_FILE_PATH, DATA_FILE_PATH, CONFIG_NAMESPACE, TARGET_SINKS, SOURCE_FILES);
    }
}
