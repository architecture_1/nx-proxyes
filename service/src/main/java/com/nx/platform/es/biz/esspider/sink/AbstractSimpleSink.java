package com.nx.platform.es.biz.esspider.sink;

/**
 * @author
 * @date 2018/01/28
 */
public abstract class AbstractSimpleSink implements Sink {

    protected final String identity;

    public AbstractSimpleSink(String identity) {
        this.identity = identity;
    }

}
