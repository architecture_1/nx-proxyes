package com.nx.platform.es.biz.esspider.sink;

import com.nx.platform.es.service.ESClientManager;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * @author
 * @since 2017年3月22日
 */
public abstract class AbstractESSink implements Sink {

    private final String identity;
    private final ESClientManager esManager;

    public AbstractESSink(String identity, ESClientManager esManager) {
        this.identity = identity;
        this.esManager = esManager;
    }

    protected Pair<RestHighLevelClient, String> getClient() {
        //旧版本的esclient， 写入只有一个集群，通过配置全部获得
        ESClientManager.ESClient clients = esManager.getResource(null, null);

        return Pair.of(clients.getClient(), clients.getHosts());
    }

}
