package com.nx.platform.es.biz.query.agg.handler;


import com.nx.platform.es.biz.query.agg.AggRequestContext;

/**
 * @author
 * @since 2016年10月15日
 */
public enum AggPageHandler implements AggRequestHandler {

    INSTANCE;

    @Override
    public void handle(AggRequestContext context) {
        context.getRequest().source().size(0);
        context.getTrace().append("&size=0");
    }

}
