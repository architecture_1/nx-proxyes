package com.nx.platform.es.bean.modle.param;

/**
 * @author
 * @since 2016年10月15日
 */
public enum ParamFieldType {

    /***/
    Float(new SimpleParser(), new FloatHandler()),
    /***/
    Number(new SimpleParser(), new NumberHandler()),
    /***/
    NumberSet(new SimpleParser(), new NumberSetHandler()),
    /***/
    SltrKeywords(new SltrKeywordsParser(), new SltrKeywordsHandler()),
    /***/
    GeoPoint(new GeoPointParser(), new GeoPointHandler());

    private final ParamFieldConfigParser parser;
    private final ParamFieldHandler handler;

    ParamFieldType(ParamFieldConfigParser parser, ParamFieldHandler handler) {
        this.parser = parser;
        this.handler = handler;
    }

    public ParamFieldConfigParser getParser() {
        return parser;
    }

    public ParamFieldHandler getHandler() {
        return handler;
    }

}
