package com.nx.platform.es.biz.esspider.handler;

import com.nx.platform.es.biz.esspider.entity.Item;

import java.util.Map;

/**
 * 数据中转处理器
 *
 * @author
 * @since 2017年3月22日
 */
public interface Handler {

    String SQL_IDS_KEY = "ids";
    String CATE_PARENT_ID = "cate_parent_id";

    /**
     * 初始化过滤处理器
     *
     * @param settings
     * @throws Exception
     */
    void init(Map<?, ?> settings) throws Exception;

    /**
     * 执行过滤处理(can not delete or add items)
     *
     * @param items
     * @return
     * @throws Exception
     */
    void handle(Map<Long, Item> items) throws Exception;

}
