package com.nx.platform.es.service.impl;

import org.springframework.stereotype.Component;

import com.nx.platform.es.biz.esspider.app.Notifyer;

/**
 * @author alex
 * @brief
 * @date 2019/11/4
 */

@Component
public class MQNotifierService extends Notifyer {

    private static final String CONFIG_FILE_PATH;
    private static final String DATA_FILE_PATH;
    private static final String CONFIG_NAMESPACE;

    static {
        String currentPath = "./";
//        String currentPath = Path.getCurrentPath();
        CONFIG_FILE_PATH = System.getProperty("config_path", currentPath + "/config");
        DATA_FILE_PATH = System.getProperty("data_path", currentPath + "/data");
        CONFIG_NAMESPACE = "es";
    }

    public MQNotifierService() {
        super(CONFIG_FILE_PATH, DATA_FILE_PATH, CONFIG_NAMESPACE);
    }

}
