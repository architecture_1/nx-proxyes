package com.nx.platform.es.common.utils;

/**
 * Apollo配置的key统一在这里获取
 */
public class Constants {

    public static final String VALIDATE_IDC = "VALIDATE_IDC";


    public static final String EXPLAIN_LEVEL = "explainlevel";
    public static final int EXPLAIN_LEVEL_DEFAULT = 9;

    public static final String ES_CLIENT = "es.clients";

    /**
     * query constant
     */
    public static final String ES_CONFIG = "es.query.configs";

    public static final String CODIS_CONFIG_KEY = "es.codis.config";
    public static final String CODIS_ZK_ADDRESS = "codis.zkaddress";
    public static final String CODIS_ZK_TIMEOUT = "codis.zkSessionTimeoutMs";
    public static final String CODIS_ZK_SO_TIMEOUT = "codis.soTimeoutMs";
    public static final String CODIS_ZK_CON_TIMEOUT = "codis.connectTimeoutMs";
    public static final String CODIS_ZK_DIR = "codis.zkdir";
    public static final String CODIS_TEAM = "codis.team";
    public static final String CODIS_APPKEY = "codis.appkey";
    public static final String CODIS_PASSWORD = "codis.password";

    public static final String CACHE_SEARCH = "cache.search";
    public static final String CACHE_COUNT = "cache.count";
    public static final String CACHE_AGG = "cache.agg";

    /**
     * write index
     */
    public static final String KEY_DB = "db";
    public static final String KEY_PROCESSORS = "processors";
    public static final String KEY_MQ = "mq";
    public static final String KEY_HANDLERS = "handlers";
    public static final String KEY_SINKS = "sinks";
    public static final String SINKS_ID = "id";
    public static final String SINKS_CLASS_NAME = "class-name";
    public static final String KEY_DEBUG = "debug";

    /**
     * processer
     */
    public static final String POOL_SIZE = "pool-size";
    public static final String FLUSH_SIZE_LIMIT = "flush-size-limit";
    public static final String FLUSH_TIME_LIMIT = "flush-time-limit";
    public static final String PROCESS_ID_SHARDING = "process-id-sharding";
    public static final String PROCESS_BULK_SIZE = "process-bulk-size";
    public static final long FLUSH_TIME_LIMIT_DEFAULT = 15000;
    public static final long FLUSH_TIME_LIMIT_MIN = 10;
    public static final long FLUSH_TIME_LIMIT_MAX = 1800000;
    public static final int FLUSH_SIZE_LIMIT_DEFAULT = 100;
    public static final int FLUSH_SIZE_LIMIT_MIN = 1;
    public static final int FLUSH_SIZE_LIMIT_MAX = 100000;
    public static final int PROCESS_BULK_SIZE_DEFAULT = 100;
    public static final int PROCESS_BULK_SIZE_MIN = 1;
    public static final int PROCESS_BULK_SIZE_MAX = 200;
    public static final int POOL_SIZE_DEFAULT = 5;
    public static final int POOL_SIZE_MIN = 1;
    public static final int POOL_SIZE_MAX = 50;
}
