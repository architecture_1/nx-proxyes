package com.nx.platform.es.common.utils;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * 常用分隔符
 * <p>
 * Created by  on 2017/6/6.
 */
public class MoreSplitters {

    public static final Pattern VERTICAL_OR_COMMA = Pattern.compile("[,|]");
    public static final Pattern VERTICAL = Pattern.compile("\\|");
    public static final Pattern COMMA = Pattern.compile(",");
    public static final Pattern EQUAL = Pattern.compile("=");
    public static final Pattern COLON = Pattern.compile(":");
    public static final Pattern BREAKLINE = Pattern.compile("[\n\r]+");
    public static final Pattern UNDERLINE = Pattern.compile("_");

    public static void main(String[] args) {
        System.out.println(Arrays.toString(VERTICAL_OR_COMMA.split("1,2|3")));
    }

}
