package com.nx.platform.es.bean.modle.rescore;

import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class SimpleParser implements RescoreFieldConfigParser {

    @Override
    public ImmutableMap<String, ?> parse(Map<?, ?> settings) {
        int windowSize = MoreMaps.getIntValue(settings, "window_size", 0);
        float queryWeight = MoreMaps.getFloatValue(settings, "query_weight", 1f);
        float rescoreQueryWeight = MoreMaps.getFloatValue(settings, "rescore_query_weight", 1f);
        String boostMode = MoreMaps.getString(settings, "boost_mode", "sum");
        String scoreMode = MoreMaps.getString(settings, "score_mode", "sum");
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put("window_size", windowSize);
        builder.put("query_weight", queryWeight);
        builder.put("rescore_query_weight", rescoreQueryWeight);
        builder.put("boost_mode", boostMode);
        builder.put("score_mode", scoreMode);
        return builder.build();
    }

}
