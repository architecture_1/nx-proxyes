package com.nx.platform.es.bean.modle.score;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.nx.platform.es.common.utils.MoreMaps;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;

import java.util.Map;

/**
 * @author
 * @since 2016年10月15日
 */
public class ScoreFunctionField {

    private final ScoreFunctionFieldType type;
    private final ImmutableMap<String, ?> config;

    private ScoreFunctionField(ScoreFunctionFieldType type, ImmutableMap<String, ?> config) {
        this.type = type;
        this.config = config;
    }

    public static ScoreFunctionField create(Map<?, ?> settings) {
        String type = MoreMaps.getString(settings, "type");
        Preconditions.checkState(!Strings.isNullOrEmpty(type), "type not found");
        ScoreFunctionFieldType fieldType = ScoreFunctionFieldType.valueOf(type);
        ImmutableMap<String, ?> config = fieldType.getParser().parse(settings);
        return new ScoreFunctionField(fieldType, config);
    }

    public FilterFunctionBuilder handle(Map<String, Object> params) {
        return type.getHandler().handle(config, params);
    }

    public ScoreFunctionFieldType getType() {
        return type;
    }

    public ImmutableMap<String, ?> getConfig() {
        return config;
    }

}
