package com.nx.platform.es.biz.query.search.cache;

import com.nx.platform.es.common.utils.Constants;
import com.nx.platform.es.system.config.ConfigCenter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @date 2018/02/28
 */
public class SearchCaches {

    private static final String DEFAULT = CodisSearchCache.class.getSimpleName();
    private static final SearchCache NULL = new NullSearchCache();
    private final Map<String, SearchCache> map;

    public SearchCaches(SearchCache... caches) {
        map = new HashMap<>();
        for (SearchCache cache : caches) {
            map.put(cache.getIdentity(), cache);
        }
    }

    @NotNull
    public SearchCache getCache() {
        return (SearchCache) ConfigCenter
                .getConfig(Constants.CACHE_SEARCH, map::get)
                .orElse(map.getOrDefault(DEFAULT, NULL));
    }

}
