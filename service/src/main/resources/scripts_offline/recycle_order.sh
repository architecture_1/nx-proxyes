#!/bin/sh
##

URL_PREFIX="http://192.168.167.224:9600"
curl -XDELETE "${URL_PREFIX}/_template/recycle_order?pretty"
## 创建模板
curl -XPUT -H Content-Type:application/json "${URL_PREFIX}/_template/recycle_order?pretty" -d '{
                     "template": "recycle_order_v*",
                     "settings": {
                       "number_of_replicas": 1,
                       "number_of_shards": 12
                     },
                     "mappings": {
                       "_doc": {
                         "properties": {
                           "buyer_id": {
                             "type": "long",
                             "null_value": 0
                           },
                           "seller_id": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "state": {
                             "type": "integer",
                             "null_value": "0"
                           },
                           "pay_state": {
                             "type": "integer",
                             "null_value": "0"
                           },
                           "recall_state": {
                             "type": "integer",
                             "null_value": "0"
                           },
                           "update_time": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "create_time": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "check_finish_time": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "order_source": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "del": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "order_id": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_1": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_2": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_3": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_4": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_5": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_6": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_7": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_8": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_9": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "long_key_10": {
                             "type": "long",
                             "null_value": "0"
                           },
                           "string_256_key_11": {
                             "type": "keyword"
                          },
                          "string_256_key_12": {
                             "type": "keyword"
                          },
                          "string_256_key_13": {
                             "type": "keyword"
                          },
                          "string_256_key_14": {
                             "type": "keyword"
                          },
                          "string_256_key_15": {
                             "type": "keyword"
                          },
                          "string_256_key_16": {
                             "type": "keyword"
                          },
                          "string_256_key_17": {
                             "type": "keyword"
                          },
                          "string_256_key_18": {
                             "type": "keyword"
                          },
                          "string_256_key_19": {
                             "type": "keyword"
                          }
                         },
                         "dynamic": false,
                         "_all": {
                           "enabled": false
                         }
                       }
                     }
                   }'
## 创建索引
curl -XDELETE "${URL_PREFIX}/recycle_order_v1?pretty"
curl -XPUT "${URL_PREFIX}/recycle_order_v1?pretty"
##创建别名
curl -XPUT "${URL_PREFIX}/recycle_order_v1/_alias/recycle_order"
##http://192.168.171.14:9200/*/_alias/recycle_order_v1  查看别名指向