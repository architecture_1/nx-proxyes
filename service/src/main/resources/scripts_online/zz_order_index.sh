#!/bin/sh
URL_PREFIX="http://10.162.17.26:6600"
curl -XDELETE "${URL_PREFIX}/_template/order?pretty"
## 创建模板
curl -H Content-Type:application/json -XPUT "${URL_PREFIX}/_template/order?pretty" -d '{
       "index_patterns": "order_v*",
       "settings": {
          "number_of_replicas": 1,
          "number_of_shards": 15
        },
       "mappings": {
          "_doc": {
            "properties": {
              "seller_id": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "buyer_id": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "buyer_del": {
                "type": "integer",
                "null_value": 0,
                "doc_values": false
              },
              "seller_del": {
                "type": "integer",
                "null_value": 0,
                "doc_values": false
              },
              "status": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "info_id": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "deliver_time": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              },
              "create_time": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              },
              "pay_time": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              },
              "order_source": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "order_type": {
                "type": "long",
                "null_value": 0,
                "doc_values": false
              },
              "logistics_num": {
                "type": "keyword",
                "null_value": 0,
                "doc_values": false
              },
              "latest_op_time": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              },
              "business_line_id":{
                 "type":"integer",
                 "null_value":0
              },
              "business_line_first":{
                 "type":"integer",
                 "null_value":0
              },
              "business_line_second":{
                 "type":"integer",
                 "null_value":0
              },
              "address":{
                  "type":"text",
                  "analyzer": "ik_max_word",
                  "search_analyzer": "ik_max_word",
                  "term_vector": "with_positions_offsets",
                  "index": "true",
                  "doc_values": false
              },
              "pay_id":{
                 "type":"long",
                 "null_value":"0"
              },
              "info_title":{
                  "type":"text",
                  "analyzer": "ik_max_word",
                  "search_analyzer": "ik_max_word",
                  "term_vector": "with_positions_offsets",
                  "index": "true",
                  "doc_values": false
              },
              "buyer_nickname":{
                  "type":"text",
                  "analyzer": "ik_max_word",
                  "search_analyzer": "ik_max_word",
                  "term_vector": "with_positions_offsets",
                  "index": "true",
                  "doc_values": false
              },
              "cate_parent_id":{
                  "type":"integer",
                  "null_value": "0"
              },
              "cate_id":{
                  "type":"integer",
                  "null_value": 0
              },
              "cate_child_id":{
                  "type":"integer",
                  "null_value": "0"
              },
              "cate_list":{
                  "type":"integer",
                  "null_value": "0"
              },
              "app_id":{
                  "type":"integer",
                  "null_value": "0"
              },
              "service_id":{
                  "type":"keyword",
                  "null_value": "0"
              },
              "buyer_comment":{
                  "type":"boolean",
                  "null_value": "false"
              },
              "seller_comment":{
                  "type":"boolean",
                  "null_value": "false"
              },
              "long_key_1": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_2": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_3": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_4": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_5": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_6": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_7": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_8": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_9": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_10": {
                 "type": "long",
                 "null_value": "0"
               },
               "long_key_11": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_12": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_13": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_14": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_15": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_16": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_17": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_18": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_19": {
                 "type": "long",
                 "null_value": "0"
              },
              "long_key_20": {
                 "type": "long",
                 "null_value": "0"
              },
               "long_key_21": {
                 "type": "long",
                 "null_value": "0"
              },
               "long_key_22": {
                 "type": "long",
                 "null_value": "0"
              },
               "long_key_23": {
                 "type": "long",
                 "null_value": "0"
              },
               "long_key_24": {
                 "type": "long",
                 "null_value": "0"
              },
               "long_key_25": {
                 "type": "long",
                 "null_value": "0"
              },
              "string_256_key_26": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_27": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_28": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_29": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_30": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_31": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_32": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_33": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_34": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_35": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_36": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_37": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_38": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_39": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_40": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_41": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_256_key_42": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_1024_key_43": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_1024_key_44": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "string_1024_key_45": {
                 "type": "text",
                 "analyzer": "ik_smart",
                 "term_vector": "with_positions_offsets"
              },
              "c_t": {
                 "type": "date",
                 "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              },
              "u_t": {
                 "type": "date",
                 "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
              }
            },
            "dynamic": false,
            "_all": {
              "enabled": false
            }
          }
      }
   }
 }'
## 创建索引
curl -XDELETE "${URL_PREFIX}/order_v1?pretty"
curl -XPUT "${URL_PREFIX}/order_v1?pretty"
##创建别名
curl -XPUT "${URL_PREFIX}/order_v1/_alias/order"
##http://10.148.15.228:5500/*/_alias/recycle_order_v1  查看别名指向