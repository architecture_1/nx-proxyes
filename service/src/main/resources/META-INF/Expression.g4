
grammar Expression;

expr  :  val op=(EQ|NE|GT|GE|LT|LE) val     # compare
      |  ID IN '[' NUM (',' NUM)* ']'       # in
      |  ID                                 # exsit
      |  '!' expr                           # not
      |  expr '&&' expr                     # and
      |  expr '||' expr                     # or
      |  '(' expr ')'                       # parensExpr
      ;
val   :  val op=(BITAND|BITOR) val          # bitAndOr
      |  val op=(MUL|DIV) val               # muldiv
      |  val op=(ADD|SUB) val               # addSub
      |  NUM                                # number
      |  ID                                 # params
      |  '(' val ')'                        # parensVal
      ;

MUL   :  '*' ;
DIV   :  '/' ;
ADD   :  '+' ;
SUB   :  '-' ;
EQ    :  '=' ;
NE    :  '!=' ;
GT    :  '>' ;
GE    :  '>=' ;
LT    :  '<' ;
LE    :  '<=' ;
IN    :  'in' ;
BITAND:  '&' ;
BITOR :  '|' ;

ID    :  [a-zA-Z_][a-zA-Z_0-9-]* ;
NUM   :  [+-]? ('0' | [1-9] (Digits? | '_'+ Digits)) [lL]?;
WS    :  [ \t\r\n]+ -> skip ;

fragment Digits
      :  [0-9] ([0-9_]* [0-9])?
      ;
