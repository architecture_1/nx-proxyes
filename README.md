## nx-esproxy-service
### 工程介绍
> nx-esproxy-service集成了nx-rpc、mybatis、sharding-jdbc，具体功能项如下：
* Spring+mybatis增、改、查demo，参考：OrderController、RefundController
* 订单表用sharding-jdbc实现了分表策略，参考：OrderAlgorithm
* 通用参数校验框架，参考：OrderController.updateOrderStatus()
* 异常统一处理，参考：ExceptionInterceptor
* 本地Properties文件支持Spring表达式，参考：LoadProperties、RefundController

### 工程目录
> com.nx.platform.es目录结构
```
├── bean        实体对象
│   ├── dto     数据传输对象
│   └── modle   内部使用的对象
│
├── common 
│   ├── constant 常量对象
│   └── utils   工具类，字符串、日期等
│
├── biz    业务处理层（非必须）
│   └── modle   与外部交互需要用到的实体
│   └── wrapper 外部RPC调用
│
├── remote  外部接口层，异常由此层捕获进行统一处理
│
├── dao  dao层，如果没有数据库操作可不需要
│
├── service     业务逻辑处理层，事务作用在此层
│   └── impl    业务逻辑接口实现
│
└── system      系统相关
    ├── config  配置文件相关
    ├── utils   系统工具，redis、Spring、scf服务等
    ├── db      数据库分表相关配置
    ├── filter  过滤器
    └── init    系统初始化
```
> 注意事项
1. 调用其它RPC服务可放到remote层，如remote中方法太长可自行增加biz层
2. 外部RPC调用不能放到service层，放在service层会增加事务时长，严重影响数据库性能
