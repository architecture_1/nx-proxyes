package com.nx.platform.es.contract;


import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;

/**
 * 计数服务，统计满足条件的所有文档的总数量
 *
 * @author zhanghuaren
 * @since 2016年10月21日
 */
@ServiceContract
public interface ICountService {

    /**
     * 获取满足条件的商品总数量
     *
     * @param logStr 日志前缀
     * @param index 索引
     * @param query 查询串( 相关字段 )
     * @return
     * @throws Exception
     */
    @OperationContract
    long count(String logStr, String index, String query) throws Exception;

}
