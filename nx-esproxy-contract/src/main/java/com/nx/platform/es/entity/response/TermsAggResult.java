package com.nx.platform.es.entity.response;


import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;

import java.util.LinkedHashMap;

/**
 * 聚合搜索结果
 * 
 * @author zhanghuaren
 * @since Sep 13, 2016
 */
@SCFSerializable(name = "dealESResult")
public class TermsAggResult {

    /**
     * 当前条件命中记录总个数
     */
    @SCFMember(orderId = 1)
    private int totalHits;

    /**
     * 聚合结果桶(如果根据以及分类聚合，则结果为LinkedHashMap<分类ID, 搜索结果集中属于此分类的数量>)
     */
    @SCFMember(orderId = 2)
    private final LinkedHashMap<String, Long> buckets;

    public TermsAggResult() {
        this(0);
    }

    public TermsAggResult(int totalHits) {
        this(totalHits, new LinkedHashMap<String, Long>(0));
    }

    public TermsAggResult(int totalHits, LinkedHashMap<String, Long> buckets) {
        this.totalHits = totalHits;
        this.buckets = buckets;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public LinkedHashMap<String, Long> getBuckets() {
        return buckets;
    }

}
