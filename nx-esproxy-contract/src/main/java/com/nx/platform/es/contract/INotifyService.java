package com.nx.platform.es.contract;


import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;

/**
 * @author alex
 * @brief
 * @date 2019/10/17
 */
@ServiceContract
public interface INotifyService {
    /**
     * 通知es间索引，和Mq效果一致
     *
     * @param logStr
     * @param message
     * @throws Exception
     */
    @OperationContract
    void notifyTest(String logStr, String message) throws Exception;
}
