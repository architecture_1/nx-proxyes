package com.nx.platform.es.entity.response;



import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 搜索命中结果集
 * 
 * @author zhanghuaren
 * @since May 20, 2016
 */
@SCFSerializable(name = "dealESSearchResult")
public class SearchResult {

    /**
     * 当前条件命中记录总个数
     */
    @SCFMember(orderId = 1)
    private final int totalHits;

    /**
     * 当前条件命中记录中当前页的结果<br/>
     * Key如无特殊说明，均为数据的ID(如帖子搜索中为帖子的ID，用户搜索中为用户的ID)<br/>
     * Value为数据附带的其它属性，如帖子搜索，如果有接口说明返回用户ID，则用户ID存储为其中的一个属性<br/>
     * 如: {"54321": {"uid":123456}}<br/>
     */
    @SCFMember(orderId = 2,generic = true)
    private final LinkedHashMap<String, Map<String, Object>> hits;

    /**
     * @param
     * @param
     */
    public SearchResult() {
        this(0, new LinkedHashMap<String, Map<String, Object>>(0));
    }

    /**
     * @param totalHits
     * @param hits
     */
    public SearchResult(int totalHits, LinkedHashMap<String, Map<String, Object>> hits) {
        this.totalHits = totalHits;
        this.hits = hits;
    }

    /**
     * @return 当前条件命中记录总个数
     */
    public int getTotalHits() {
        return totalHits;
    }

    /**
     * 当前条件命中记录中当前页的结果<br/>
     * 如: {"id": {"uid":123456}}
     * 
     * @return
     */
    public LinkedHashMap<String, Map<String, Object>> getHits() {
        return hits;
    }

    /**
     * 当前条件命中记录中当前页的ID列表
     * 
     * @return
     */
    public String[] getIds() {
        if (hits == null) {
            throw new NullPointerException("hits is null");
        }
        return hits.keySet().toArray(new String[hits.keySet().size()]);
    }

}
