package com.nx.platform.es.contract;

import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;
import com.nx.platform.es.entity.response.SearchResult;

/**
 * 搜索服务，查询满足条件的所有文档
 *
 * @author zhanghuaren
 * @since 2016年10月16日
 */
@ServiceContract
public interface ISearchService {

    /**
     * 查询接口
     *
     * @param logStr 日志前缀
     * @param index 索引
     * @param query 查询串(筛选传参+固定传参(from|size|page|sort))
     * @param params 排序传参(如经纬度)
     * @return
     * @throws Exception
     */
    @OperationContract
    SearchResult search(String logStr, String index, String query, String params) throws Exception;

    /**
     * 帖子搜索，返回结果中带UID等
     *
     * @param logStr 日志前缀
     * @param index 索引
     * @param query 查询串( 固定字段 from|size|page|sort + 相关字段 )
     * @param params 排序时需要用到的参数 (如 经纬度等)
     * @return
     * @throws Exception
     */
    @OperationContract
    SearchResult searchWithFields(String logStr, String index, String query, String params) throws Exception;

}
