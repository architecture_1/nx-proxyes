package com.nx.platform.es.contract;


import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;
import com.nx.platform.es.entity.response.TermsAggResult;

/**
 * 聚合服务，对满足条件的所有文档进行聚合统计
 *
 * @author zhanghuaren
 * @since 2016年10月16日
 */
@ServiceContract
public interface IAggService {

    /**
     * 聚合接口(只支持指定数字类型字段(具体见接口说明)聚合)
     *
     * @param logStr 日志前缀
     * @param index 索引
     * @param query 查询串(筛选传参+固定传参(from|size|page|sort))
     * @param params 聚合信息字段，必须指定字段名{@code term}，可指定排序方式{@code count|term}
     * <ul>
     * <li>如：term=cate_parent_id</li>
     * <li>如：term=cate_parent_id&sort=count</li>
     * <li>如：term=cate_parent_id&sort=count_desc</li>
     * <li>如：term=cate_parent_id&sort=count_asc</li>
     * <li>如：term=cate_parent_id&sort=term</li>
     * <li>如：term=cate_parent_id&sort=term_desc</li>
     * <li>如：term=cate_parent_id&sort=term_asc</li>
     * </ul>
     * @return
     * @throws Exception
     */
    @OperationContract
    TermsAggResult termsAgg(String logStr, String index, String query, String params) throws Exception;

}
